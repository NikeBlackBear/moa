package com.loginsoft.moabj.service;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import com.loginsoft.moabj.R;
import com.loginsoft.moabj.Util.LogUtil;
import com.loginsoft.moabj.Util.MoaBJPreferenceManager;
import com.loginsoft.moabj.common.CommonValues;
import com.loginsoft.moabj.main.MoaBJMainFrame;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by Administrator on 2015-02-05.
 */
public class MoaService extends Service implements CommonValues {
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private NotificationManager mNotificationManager;
    private final int NOTIFICATION_ID = 1;
    private Bitmap mMoaBjlogo;
    private GregorianCalendar mCalendar;
    private AlarmManager mAlarmManager;

    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    public void init() {
        IntentFilter alarmIntentFilter = new IntentFilter();
        alarmIntentFilter.addAction(ACTION_ALARAM_STATE_CHANGED);
        alarmIntentFilter.addAction(ACTION_ALARM_NOTIFICATION);
        registerReceiver(mAlarmReceiver, alarmIntentFilter);

        mAlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        setAlarm();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mAlarmReceiver);
    }

    // 알람 등록
    private void setAlarm() {
        mCalendar = new GregorianCalendar();
        int year = Calendar.getInstance(Locale.KOREA).get(Calendar.YEAR);
        int month = Calendar.getInstance(Locale.KOREA).get(Calendar.MONTH);
        LogUtil.LOGE("hsj :: setAlarm :: month 확인 :: " + month);
        int day = Calendar.getInstance(Locale.KOREA).get(Calendar.DAY_OF_MONTH);
        mCalendar.set(year, month, day, 22, 0);
        long setTime;
        if (System.currentTimeMillis() > mCalendar.getTimeInMillis()) {
            setTime = mCalendar.getTimeInMillis() + 86400000;
        } else {
            setTime = mCalendar.getTimeInMillis();
        }
        LogUtil.LOGE("hsj :: setAlarm :: mCalendar 확인 :: " + mCalendar.getTimeInMillis());
        LogUtil.LOGE("hsj :: setAlarm :: setTime 확인 :: " + setTime);
        LogUtil.LOGE("hsj :: setAlarm :: 현재시간 확인 :: " + System.currentTimeMillis());
        mAlarmManager.setRepeating(AlarmManager.RTC, setTime, 86400000, pendingIntent());
    }

    //알람 해제
    private void resetAlarm() {
        mAlarmManager.cancel(pendingIntent());
    }

    private PendingIntent pendingIntent() {
        Intent intent = new Intent(ACTION_ALARM_NOTIFICATION);
        PendingIntent pi = PendingIntent.getBroadcast(this, 0, intent, 0);
        return pi;
    }

    private void sendMessageNotification() {
        mMoaBjlogo = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent intent = new Intent(this, MoaBJMainFrame.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                intent, 0);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        if (mMoaBjlogo != null) {
            mBuilder.setLargeIcon(mMoaBjlogo);
        }
        mBuilder.setSmallIcon(R.drawable.ic_launcher);
        mBuilder.setContentTitle(getResources().getString(R.string.app_name));
        mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(getResources().getString(R.string.app_name)));
        mBuilder.setContentText(getResources().getString(R.string.str_alarm_text));

        mBuilder.setContentIntent(contentIntent);
        mBuilder.setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    private final BroadcastReceiver mAlarmReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ACTION_ALARAM_STATE_CHANGED)) {
                LogUtil.LOGE("hsj :: BroadcastReceiver :: 알람세팅에 관한 브로드캐스트");
                if (MoaBJPreferenceManager.getAlarmState(MoaService.this).equals(ALARM_SWITCH_ON)) {
                    setAlarm();
                } else if (MoaBJPreferenceManager.getAlarmState(MoaService.this).equals(ALARM_SWITCH_OFF)) {
                    resetAlarm();
                }
            } else if (intent.getAction().equals(ACTION_ALARM_NOTIFICATION)) {
                LogUtil.LOGE("hsj :: BroadcastReceiver :: 세팅한 알람 시간에 보내는 브로드캐스트");
                sendMessageNotification();
            }
        }
    };
}
