package com.loginsoft.moabj.http;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.loginsoft.moabj.Util.LogUtil;
import com.loginsoft.moabj.common.CommonValues;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class ServerProxy implements RequestURL, CommonValues {
    public static final String TAG = ServerProxy.class.getSimpleName();
    private static ServerProxy mInstance;
    private ResponseCallback mResponseCallback;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    public static ServerProxy getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new ServerProxy(context);
        }
        return mInstance;
    }

    private ServerProxy(Context context) {
        mRequestQueue = Volley.newRequestQueue(context);
        mImageLoader = new ImageLoader(this.mRequestQueue, new ImageLoader.ImageCache() {
            private final LruCache mCache = new LruCache(10);

            public void putBitmap(String url, Bitmap bitmap) {
                mCache.put(url, bitmap);
            }

            public Bitmap getBitmap(String url) {
                return (Bitmap) mCache.get(url);
            }
        });
    }


    public interface ResponseCallback {
        void onResponse(String response);

        void onError(int status);
    }

    public ImageLoader getImageLoader() {
        return mImageLoader;
    }

    public RequestQueue getRequestQueue() {
        return mRequestQueue;
    }

    ;

    //HOT 동영상 영상 목록 가져오는 메소드
    public void requestHotVideoList(final ResponseCallback callback, int count) {
        LogUtil.LOGE("ServerProxy :: requestHotVideoList() :: count = " + count);
        RequestParams params = new RequestParams();
        params.setTargetURL(URL_GET_VIDEO_HOT);
        params.put(PARAM_COUNT, count);

        StringRequest request = new StringRequest(Request.Method.GET, params.buildRequestURL(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        callback.onResponse(response);
                    }
                }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError e) {
                if (e != null && e.networkResponse != null)
                    callback.onError(e.networkResponse.statusCode);
            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(
                    NetworkResponse response) {
                try {
                    String string = new String(response.data, "UTF-8");
                    return Response.success(string,
                            HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.parseNetworkResponse(response);
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(request);
    }

    public void requestBJList(final ResponseCallback callback, int startnum, int count) {
        requestBJList(callback, startnum, count, TYPE_NON_PARTNER);
    }

    //BJ 리스트 가져오는 메소드
    public void requestBJList(final ResponseCallback callback, int startnum, int count, String partnerType) {
        LogUtil.LOGE("ServerProxy :: requestBJList() :: startnum = " + startnum + " count = " + count);
        RequestParams params = new RequestParams();
        params.setTargetURL(URL_GET_BJ_LIST);
        params.put(PARAM_PAGE_START_NUM, startnum);
        params.put(PARAM_BJ_COUNT, count);
        params.put(PARAM_BJ_PARTNER_TYPE, partnerType);

        StringRequest request = new StringRequest(Request.Method.GET, params.buildRequestURL(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        callback.onResponse(response);
                    }
                }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError e) {
                if (e != null && e.networkResponse != null)
                    callback.onError(e.networkResponse.statusCode);
            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(
                    NetworkResponse response) {
                try {
                    String string = new String(response.data, "UTF-8");
                    return Response.success(string,
                            HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.parseNetworkResponse(response);
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(request);
    }

    //BJ 이름을 검색해서 가져오는 메소드
    public void requestSerchBJ(final ResponseCallback callback, String searchName) {
        LogUtil.LOGE("ServerProxy :: requestSerchBJ() :: searchName = " + searchName);
        RequestParams params = new RequestParams();
        params.setTargetURL(URL_GET_SEARCH_BJ);
        try {
            searchName = URLEncoder.encode(searchName, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        params.put(PARAM_SEARCH_STR, searchName);

        StringRequest request = new StringRequest(Request.Method.GET, params.buildRequestURL(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        callback.onResponse(response);
                    }
                }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError e) {
                if (e != null && e.networkResponse != null)
                    callback.onError(e.networkResponse.statusCode);
            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(
                    NetworkResponse response) {
                try {
                    String string = new String(response.data, "UTF-8");
                    return Response.success(string,
                            HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.parseNetworkResponse(response);
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(request);
    }

    //해당 BJ 채널리스트를 가져오는 메소드
    public void requestTargetBJChannelList(final ResponseCallback callback, String channelID, String nextToken) {
        LogUtil.LOGE("ServerProxy :: requestTargetBJChannelList() :: channelID = " + channelID);
        RequestParams params = new RequestParams();
        params.setTargetURL(URL_GET_PLAY_LIST);
        params.put(PARAM_CHANNEL_ID, channelID);
        params.put(PARAM_PAGE_TOKEN, nextToken);

        StringRequest request = new StringRequest(Request.Method.GET, params.buildRequestURL(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        callback.onResponse(response);
                    }
                }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError e) {
                if (e != null && e.networkResponse != null)
                    callback.onError(e.networkResponse.statusCode);
            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(
                    NetworkResponse response) {
                try {
                    String string = new String(response.data, "UTF-8");
                    return Response.success(string,
                            HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.parseNetworkResponse(response);
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(request);
    }

    //해당 채널의 비디오 리스트를 가져오는 메소드
    public void requestVideoList(final ResponseCallback callback, String playlistID, String nextToken) {
        LogUtil.LOGE("ServerProxy :: requestVideoList() :: playlistID = " + playlistID);
        RequestParams params = new RequestParams();
        params.setTargetURL(URL_GET_VIDEO_LIST);
        params.put(PARAM_PLAY_LIST_ID, playlistID);
        params.put(PARAM_PAGE_TOKEN, nextToken);

        StringRequest request = new StringRequest(Request.Method.GET, params.buildRequestURL(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        callback.onResponse(response);
                    }
                }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError e) {
                if (e != null && e.networkResponse != null)
                    callback.onError(e.networkResponse.statusCode);
            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(
                    NetworkResponse response) {
                try {
                    String string = new String(response.data, "UTF-8");
                    return Response.success(string,
                            HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.parseNetworkResponse(response);
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(request);
    }

    //해당 채널의 비디오 리스트를 가져오는 메소드
    public void requestFullVideoList(final ResponseCallback callback, String channelID, String nextToken) {
        LogUtil.LOGE("ServerProxy :: requestFullVideoList() :: playlistID = " + channelID);
        RequestParams params = new RequestParams();
        params.setTargetURL(URL_GET_FULL_VIDEO_LIST);
        params.put(PARAM_CHANNEL_ID, channelID);
        params.put(PARAM_PAGE_TOKEN, nextToken);

        StringRequest request = new StringRequest(Request.Method.GET, params.buildRequestURL(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        callback.onResponse(response);
                    }
                }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError e) {
                if (e != null && e.networkResponse != null)
                    callback.onError(e.networkResponse.statusCode);
            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(
                    NetworkResponse response) {
                try {
                    String string = new String(response.data, "UTF-8");
                    return Response.success(string,
                            HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.parseNetworkResponse(response);
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(request);
    }

    //어플리케이션 버전을 가져오는 메소드
    public void requestAppVersion(final ResponseCallback callback) {
        LogUtil.LOGE("ServerProxy :: requestAppVersion()");
        RequestParams params = new RequestParams();
        params.setTargetURL(URL_GET_APPLICATION_VERSION);

        StringRequest request = new StringRequest(Request.Method.GET, params.buildRequestURL(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        callback.onResponse(response);
                    }
                }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError e) {
                if (e != null && e.networkResponse != null)
                    callback.onError(e.networkResponse.statusCode);
            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(
                    NetworkResponse response) {
                try {
                    String string = new String(response.data, "UTF-8");
                    return Response.success(string,
                            HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.parseNetworkResponse(response);
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(request);
    }

    public void requestViewCountUp(String videoId, String bjicon) {
        LogUtil.LOGE("ServerProxy :: requestViewCountUp()");
        RequestParams params = new RequestParams();
        params.setTargetURL(URL_SET_VIDEO_VIEW_COUNT);
        params.put(PARAM_VIDEO_ID, videoId);
        params.put(PARAM_BJ_ICON, bjicon);

        StringRequest request = new StringRequest(Request.Method.GET, params.buildRequestURL(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                    }
                }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError e) {
            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(
                    NetworkResponse response) {
                try {
                    String string = new String(response.data, "UTF-8");
                    return Response.success(string,
                            HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.parseNetworkResponse(response);
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(request);
    }

    public void requestNotice(final ResponseCallback callback) {
        LogUtil.LOGE("ServerProxy :: requestAppVersion()");

        StringRequest request = new StringRequest(Request.Method.POST, URL_GET_NOTICE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        callback.onResponse(response);
                    }
                }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError e) {
                if (e != null && e.networkResponse != null)
                    callback.onError(e.networkResponse.statusCode);
            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(
                    NetworkResponse response) {
                try {
                    String string = new String(response.data, "UTF-8");
                    return Response.success(string,
                            HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.parseNetworkResponse(response);
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(request);
    }
}
