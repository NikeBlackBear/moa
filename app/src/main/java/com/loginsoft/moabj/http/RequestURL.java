package com.loginsoft.moabj.http;

/**
 * Created by Administrator on 2015-02-04.
 */
public interface RequestURL {
    public static final String URL_GET_BJ_LIST = "http://loginsoftceo.cafe24.com/moabj_v1/android_get_bj_list.php";
    public static final String URL_GET_PLAY_LIST = "http://loginsoftceo.cafe24.com/moabj_v1/android_get_play_list.php";
    public static final String URL_GET_VIDEO_LIST = "http://loginsoftceo.cafe24.com/moabj_v1/android_get_video_list.php";
    public static final String URL_GET_VIDEO_HOT = "http://loginsoftceo.cafe24.com/moabj_v1/android_get_video_hot.php";
    public static final String URL_SET_VIDEO_VIEW_COUNT = "http://loginsoftceo.cafe24.com/moabj_v1/android_viewcountup.php";
    public static final String URL_GET_APPLICATION_VERSION = "http://loginsoftceo.cafe24.com/moabj_v1/android_version_view.php";
    public static final String URL_GET_NOTICE = "http://loginsoftceo.cafe24.com/moabj_v1/android_get_notice.php";
    public static final String URL_GET_SEARCH_BJ = "http://loginsoftceo.cafe24.com/moabj_v1/android_get_bj_search.php";
    public static final String URL_GET_FULL_VIDEO_LIST = "http://loginsoftceo.cafe24.com/moabj_v1/android_get_channelsearch_video_list.php";
}
