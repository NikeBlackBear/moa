package com.loginsoft.moabj.http;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.loginsoft.moabj.Util.LogUtil;
import com.loginsoft.moabj.common.CommonValues;
import com.loginsoft.moabj.database.DataManager;
import com.loginsoft.moabj.model.BJInfo;
import com.loginsoft.moabj.model.HotVideo;
import com.loginsoft.moabj.model.PlayListInfo;

import java.util.ArrayList;

/**
 * Created by Administrator on 2015-02-04.
 */
public class ParseManager implements CommonValues {

    public ParseManager() {

    }

    public ArrayList<HotVideo> parseHotVideoList(String response, DataManager dataManager) {
        ArrayList<HotVideo> hotVideoList = new ArrayList<HotVideo>();
        Gson gson = new Gson();
        hotVideoList = gson.fromJson(response, new TypeToken<ArrayList<HotVideo>>() {
        }.getType());
        LogUtil.LOGV("ParseManager :: parserHotVideoList() :: list size = " + hotVideoList.size());
        LogUtil.LOGE(gson.toJson(hotVideoList));
        for (int i = 0; i < hotVideoList.size(); i++) {
            HotVideo item = hotVideoList.get(i);
            if (dataManager.isAlreadyFavoriteVideo(item.getVideoID())) {
                item.setFavorite(true);
                hotVideoList.set(i, item);
            }
        }

        return hotVideoList;
    }

    public ArrayList<BJInfo> parseBJList(String response, DataManager dataManager) {
        ArrayList<BJInfo> bjinfolist = new ArrayList<BJInfo>();
        Gson gson = new Gson();
        bjinfolist = gson.fromJson(response, new TypeToken<ArrayList<BJInfo>>() {
        }.getType());
        LogUtil.LOGV("ParseManager :: parseHotVideoList() :: list size = " + bjinfolist.size());
        LogUtil.LOGE(gson.toJson(bjinfolist));
        for (int i = 0; i < bjinfolist.size(); i++) {
            BJInfo item = bjinfolist.get(i);
            if (dataManager.isAlreadyFavoriteBJ(item.getBJChannelID())) {
                item.setFavorite(true);
                bjinfolist.set(i, item);
            }
        }
        return bjinfolist;
    }

    public ArrayList<PlayListInfo> parsePlayList(String response) {
        Gson gson = new Gson();
        ArrayList<PlayListInfo> playList = gson.fromJson(response, new TypeToken<ArrayList<PlayListInfo>>() {
        }.getType());
        LogUtil.LOGV("ParseManager :: parsePlayList() :: list size = " + playList.size());
        LogUtil.LOGE(gson.toJson(playList));
        return playList;
    }

    public String parseAppVersion(String response) {
        JsonParser parser = new JsonParser();
        JsonArray array = parser.parse(response).getAsJsonArray();
        return array.get(0).getAsJsonObject().get(PARAM_VERSION).getAsString();
    }

    public String parseDeleteSubscriptionID(String subscriptionResonse) {
        JsonParser jsonParser = new JsonParser();
        JsonObject jsonObject = (JsonObject) jsonParser.parse(subscriptionResonse);
        LogUtil.LOGE("jsonObject :: " + jsonObject);
        if (jsonObject.getAsJsonObject("pageInfo").getAsJsonObject().get("totalResults").getAsInt() == 0) {
            LogUtil.LOGE("totalResults :: " + jsonObject.getAsJsonObject("pageInfo").getAsJsonObject().get("totalResults").getAsInt());
            return NO_HAS_SUBSCRIPTION;
        } else {
            JsonArray jsonArray = jsonObject.getAsJsonArray("items");
            JsonObject jsonObject1 = ((JsonObject) jsonArray.get(0));
            return jsonObject1.get("id").getAsString();
        }
    }

    public int parseSubscriptionValue(String subscriptionResponse) {
        JsonParser jsonParser = new JsonParser();
        JsonObject jsonObject = (JsonObject) jsonParser.parse(subscriptionResponse);
        JsonObject jsonObject1 = jsonObject.getAsJsonObject("pageInfo");
        return jsonObject1.get("totalResults").getAsInt();
    }
}
