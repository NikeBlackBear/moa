package com.loginsoft.moabj.main;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.Scopes;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.YouTubeScopes;
import com.google.api.services.youtube.model.ResourceId;
import com.google.api.services.youtube.model.Subscription;
import com.google.api.services.youtube.model.SubscriptionListResponse;
import com.google.api.services.youtube.model.SubscriptionSnippet;
import com.loginsoft.moabj.R;
import com.loginsoft.moabj.Util.LogUtil;
import com.loginsoft.moabj.Util.MoaBJPreferenceManager;
import com.loginsoft.moabj.Util.Utils;
import com.loginsoft.moabj.common.CommonValues;
import com.loginsoft.moabj.custom.CircleNetworkImageView;
import com.loginsoft.moabj.custom.ImageViewer;
import com.loginsoft.moabj.custom.LoadMoreListView;
import com.loginsoft.moabj.custom.PlayListAdapter;
import com.loginsoft.moabj.custom.TypefaceTextView;
import com.loginsoft.moabj.custom.VideoListAdapter;
import com.loginsoft.moabj.database.DataManager;
import com.loginsoft.moabj.http.ParseManager;
import com.loginsoft.moabj.http.ServerProxy;
import com.loginsoft.moabj.model.BJInfo;
import com.loginsoft.moabj.model.HotVideo;
import com.loginsoft.moabj.model.PlayListInfo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Administrator on 2015-02-10.
 */
public class MoaBJDetailBJActivity extends Activity implements CommonValues {

    private final int MODE_PLAYLIST = 100;
    private final int MODE_VIDEOLIST = 101;
    private final int MODE_FULL_VIDEOLIST = 102;

    private final int EXTENSION_SIMPLE = 200;
    private final int EXTENSION_DETAIL = 201;
    private final int REQUEST_AUTHORIZATION = 3;

    private final String KILLO = "K";
    private final String MEGA = "M";
    private final String COMMAND_CHECK_SUBSCRIPTION = "check";
    private final String COMMAND_SUBSCRIPTION = "sub";
    private final String COMMAND_DISSUBSCRIPTION = "dis";

    private LinearLayout mBackView;
    private TypefaceTextView mTitleBJView;
    private RelativeLayout mBJContent;
    private ImageViewer mBackgroundBJImage;
    private ImageView mYoutubeSubscriptionBtn;
    private ImageView mGoAfreecaBtn;
    private ImageView mFavoriteBtn;
    private ImageView mExtensionView;
    private ImageView mTabVideoListBtn;
    private ImageView mTabPlayListBtn;
    private CircleNetworkImageView mMainBJImage;
    private TypefaceTextView mBJNameView;
    private TypefaceTextView mBJDescription;
    private TypefaceTextView mSubscriptionCount;
    private TypefaceTextView mVideosCount;
    private TypefaceTextView mHitsCount;
    private RelativeLayout mLoadingView;
    private ImageView mLoadingImg;
    private LoadMoreListView mPlayListView;
    private ImageView mPartnerBJCircleRing;
    private ImageView mPartnerBJSmallCircle;

    private PlayListAdapter mPlayListAdapter;
    private VideoListAdapter mBJVideoListAdapter;

    private DataManager mDataManager;

    private BJInfo mTargetBJInfo = new BJInfo();
    private ArrayList<PlayListInfo> mTargetPlayListInfo;
    private ArrayList<HotVideo> mTargetVideoListInfo;
    private ArrayList<HotVideo> mTargetFullVideoListInfo;
    private AdView mAdView;

    private int mListMode = MODE_PLAYLIST;
    private int mExtensionMode = EXTENSION_DETAIL;
    private int mSelectPlayListIdx = -1;
    private int mCheckSubscription = 0;

    private float mDeviceDensity = 0.0f;

    private String mSubscriptionOption = null;

    private boolean mIsFinished = false;

    private GoogleAccountCredential mGoogleCredential;
    private final JsonFactory mJsonFactory = new GsonFactory();
    private final HttpTransport mTransport = AndroidHttp.newCompatibleTransport();

    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_BOOKMARK_CHANGED);
        registerReceiver(mBookMarkChangedReceiver, filter);

        setContentView(R.layout.activity_detail_bj);
        LogUtil.LOGD("DetailBJActivity :: getAccountName() :: " + MoaBJPreferenceManager.getAccountName(this));
        init();

        mTracker = ((MoaBJAnalystics) getApplication()).getTracker(this, MoaBJAnalystics.TrackerName.APP_TRACKER);
        mTracker.setScreenName("MoaBJDetailBJActivity");
        mTracker.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mBookMarkChangedReceiver);

        if (mDataManager != null) {
            mDataManager.close();
        }
        mIsFinished = true;
    }

    private void init() {
        WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        manager.getDefaultDisplay().getMetrics(metrics);
        mDeviceDensity = metrics.density;
        LogUtil.LOGE("density = " + mDeviceDensity);

        mDataManager = new DataManager(this);
        mBackView = (LinearLayout) findViewById(R.id.back_section);
        mTitleBJView = (TypefaceTextView) findViewById(R.id.detail_title_view);
        mBackgroundBJImage = (ImageViewer) findViewById(R.id.frame_bj_image);
        mBJContent = (RelativeLayout) findViewById(R.id.bj_content);
        mYoutubeSubscriptionBtn = (ImageView) findViewById(R.id.youtube_subscription_btn);
        mGoAfreecaBtn = (ImageView) findViewById(R.id.go_afreeca_btn);
        mFavoriteBtn = (ImageView) findViewById(R.id.favorite_btn);
        mMainBJImage = (CircleNetworkImageView) findViewById(R.id.bj_profile_image);
        mBJNameView = (TypefaceTextView) findViewById(R.id.bj_name);
        mBJDescription = (TypefaceTextView) findViewById(R.id.bj_description);
        mExtensionView = (ImageView) findViewById(R.id.extension);
        mTabPlayListBtn = (ImageView) findViewById(R.id.playlist_btn);
        mTabVideoListBtn = (ImageView) findViewById(R.id.videolist_btn);
        mSubscriptionCount = (TypefaceTextView) findViewById(R.id.fllowers_count);
        mVideosCount = (TypefaceTextView) findViewById(R.id.videos_count);
        mHitsCount = (TypefaceTextView) findViewById(R.id.hits_count);
        mPlayListView = (LoadMoreListView) findViewById(R.id.playList);
        mLoadingView = (RelativeLayout) findViewById(R.id.loading_view);
        mLoadingImg = (ImageView) findViewById(R.id.loadingImg);
        mAdView = (AdView) findViewById(R.id.adView);
        mPartnerBJCircleRing = (ImageView) findViewById(R.id.partnerBJRing);
        mPartnerBJSmallCircle = (ImageView) findViewById(R.id.partnerBJSmallCircle);

        mExtensionView.setOnClickListener(mExtensionBtnListener);
        mTabPlayListBtn.setOnClickListener(mTabListBtnListener);
        mTabVideoListBtn.setOnClickListener(mTabListBtnListener);
        mPlayListView.setOnItemClickListener(mListItemClickListener);
        mPlayListView.setOnLoadMoreListener(mListMoreListener);
        mBackView.setOnClickListener(mBackBtnListener);
        mYoutubeSubscriptionBtn.setOnClickListener(mSubBtnListener);
        mGoAfreecaBtn.setOnClickListener(mSubBtnListener);
        mFavoriteBtn.setOnClickListener(mSubBtnListener);

        mBJDescription.setSelected(true);

        mPlayListView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (mLoadingView.getVisibility() == View.VISIBLE) {
                    return true;
                }

                return false;
            }
        });
        mLoadingImg.setBackgroundResource(R.drawable.loadinganimationblack);

        loadBJInfo();

        if (mTargetBJInfo.getType().equals(HAS_PLAY_LIST)) {
            mTabPlayListBtn.setSelected(true);
            ServerProxy.getInstance(this).requestTargetBJChannelList(mChannelListResponseCallback, mTargetBJInfo.getBJChannelID(), "");
        } else if (mTargetBJInfo.getType().equals(NO_PLAY_LIST)) {
            mTabVideoListBtn.setSelected(true);
            mListMode = MODE_FULL_VIDEOLIST;
            ServerProxy.getInstance(this).requestFullVideoList(mFullVideoListResponseCallback, mTargetBJInfo.getBJChannelID(), "");
        }
        mSubscriptionOption = COMMAND_CHECK_SUBSCRIPTION;
        if (!MoaBJPreferenceManager.getAccountName(this).equals(SIGN_OUT_STATE)) {
            new SubscriptTask().execute();
        }
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        showLoadingView();
    }

    private void loadBJInfo() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            mTargetBJInfo = (BJInfo) intent.getSerializableExtra(EXTRA_BJ_ITEM);
            updateTitle(mTargetBJInfo.getBjName());
            mBackgroundBJImage.setImageURL(mTargetBJInfo.getThumnailurl());
            mMainBJImage.loadCircleBitmap(mTargetBJInfo.getThumnailurl());
            mBJNameView.setText(mTargetBJInfo.getBjName());
            mBJDescription.setText(mTargetBJInfo.getbjsogae().equals("") ? getText(R.string.str_empty_bj_desc) : mTargetBJInfo.getbjsogae());
            LogUtil.LOGE("MoaBJDetailBJActivity :: loadBJInfo() :: type = " + mTargetBJInfo.getPartnertype() + " type2 = " + mTargetBJInfo.getType());
            if (mTargetBJInfo.getPartnertype().equals(TYPE_PARTNER)) {
                mPartnerBJCircleRing.setVisibility(View.VISIBLE);
                mPartnerBJSmallCircle.setVisibility(View.VISIBLE);
            }
            if (mTargetBJInfo.isFavorite()) {
                mFavoriteBtn.setImageResource(R.drawable.bookmark_btn_on);
            } else {
                mFavoriteBtn.setImageResource(R.drawable.bookmark_btn_off);
            }

            if (mTargetBJInfo.getAfracaURL().equals("")) {
                mGoAfreecaBtn.setVisibility(View.GONE);
            }
        } else {
            LogUtil.LOGD("MoaBJDetailBJActivity :: loadBJInfo() :: intent null");
        }
    }

    private void showLoadingView() {
        mLoadingView.setVisibility(View.VISIBLE);
        ((AnimationDrawable) mLoadingImg.getBackground()).start();
    }

    private void dismissLoadingView() {
        mLoadingView.setVisibility(View.GONE);
        ((AnimationDrawable) mLoadingImg.getBackground()).stop();
    }

    public DataManager getDataManager() {
        return mDataManager;
    }

    private String convertCount(String targetValue) {
        LogUtil.LOGD("MoaBJDetailBJActivity :: convertCount() :: targetValue = " + targetValue);
        StringBuilder builder = new StringBuilder();
        String unitType = "";
        int subsIdx = -1;
        int value = 0;
        try {
            value = Integer.parseInt(targetValue);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return "?";
        }

        if (value < 1000) {
            LogUtil.LOGD("MoaBJDetailBJActivty :: convertCount() :: result = " + targetValue);
            return targetValue;
        } else if (value < 1000000) {
            subsIdx = targetValue.length() - 3;
            unitType = KILLO;
        } else {
            subsIdx = targetValue.length() - 6;
            unitType = MEGA;
        }

        builder.append(targetValue.substring(0, subsIdx));
        if (targetValue.charAt(subsIdx) != '0') {
            builder.append(".").append(targetValue.charAt(subsIdx));
        }
        builder.append(unitType);

        LogUtil.LOGD("MoaBJDetailBJActivty :: convertCount() :: result = " + builder.toString());
        return builder.toString();
    }

    private String getTotalVideoCount() {
        if (mListMode == MODE_FULL_VIDEOLIST) {
            if (mTargetFullVideoListInfo == null) {
                return "?";
            } else {
                return mTargetFullVideoListInfo.size() > 0 ? mTargetFullVideoListInfo.get(0).getTotalResult() : "?";
            }
        }

        int total = 0;
        for (int i = 0; i < mTargetPlayListInfo.size(); i++) {
            total += Integer.parseInt(mTargetPlayListInfo.get(i).getItemCount());
        }

        LogUtil.LOGD("MoaBJDetailBJActivity :: getTotalVideoCount() :: total = " + total);
        return String.valueOf(total);
    }

    private void updateTitle(String wantTitle) {
        mTitleBJView.setText(wantTitle);
    }

    @Override
    public void onBackPressed() {
        if (mListMode == MODE_PLAYLIST || mListMode == MODE_FULL_VIDEOLIST) {
            finish();
        } else if (mListMode == MODE_VIDEOLIST) {
            mListMode = MODE_PLAYLIST;
            updateTitle(mTargetBJInfo.getBjName());
            mPlayListView.setAdapter(mPlayListAdapter);
        }
    }

    private void sendBookMarkBroadcast(String bookMarkActionType, String bookMarkContent, String id) {
        Intent intent = new Intent(ACTION_BOOKMARK_CHANGED);
        intent.putExtra(EXTRA_BOOKMARK_CONTENT_TYPE, bookMarkContent);
        intent.putExtra(EXTRA_BOOKMARK_ACTION_TYPE, bookMarkActionType);
        intent.putExtra(EXTRA_BOOKMARK_ID, id);
        sendBroadcast(intent);
    }

    private final LoadMoreListView.OnLoadMoreListener mListMoreListener = new LoadMoreListView.OnLoadMoreListener() {
        @Override
        public void onLoadMore() {
            LogUtil.LOGE("MoaBJDetailBJActivity :: mListMoreListener :: mListMode = " + mListMode);
            if (mListMode == MODE_VIDEOLIST) {
                String nextToken = mTargetVideoListInfo.get(mTargetVideoListInfo.size() - 1).getNextpagetoken();
                if (!nextToken.equals(VALUE_EMPTY)) {
                    ServerProxy.getInstance(MoaBJDetailBJActivity.this).requestVideoList(mVideoListMoreResponseCallback, mTargetPlayListInfo.get(mSelectPlayListIdx).getPlaylistID(), nextToken);
                } else {
                    mPlayListView.onLoadMoreComplete();
                }
            } else if (mListMode == MODE_PLAYLIST) {
                String nextToken = mTargetPlayListInfo.get(mTargetPlayListInfo.size() - 1).getNextpagetoken();
                if (!nextToken.equals(VALUE_EMPTY)) {
                    ServerProxy.getInstance(MoaBJDetailBJActivity.this).requestTargetBJChannelList(mChannelListMoreResponseCallback, mTargetBJInfo.getBJChannelID(), nextToken);
                } else {
                    mPlayListView.onLoadMoreComplete();
                }
            } else if (mListMode == MODE_FULL_VIDEOLIST) {
                LogUtil.LOGE("mTargetFullVideoListInfo = " + mTargetFullVideoListInfo);
                String nextToken = mTargetFullVideoListInfo.get(mTargetFullVideoListInfo.size() - 1).getNextpagetoken();
                if (!nextToken.equals(VALUE_EMPTY)) {
                    ServerProxy.getInstance(MoaBJDetailBJActivity.this).requestFullVideoList(mFullVideoMoreListResponseCallback, mTargetBJInfo.getBJChannelID(), nextToken);
                } else {
                    mPlayListView.onLoadMoreComplete();
                }
            }
        }
    };

    private final AdapterView.OnItemClickListener mListItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (mListMode == MODE_PLAYLIST) {
                if (mTargetPlayListInfo == null) {
                    return;
                }

                mSelectPlayListIdx = position;
                updateTitle(mTargetBJInfo.getBjName() + " - " + mTargetPlayListInfo.get(position).getTitle());
                ServerProxy.getInstance(MoaBJDetailBJActivity.this).requestVideoList(mVideoListResponseCallback, mTargetPlayListInfo.get(position).getPlaylistID(), "");
                mListMode = MODE_VIDEOLIST;
                showLoadingView();
            } else if (mListMode == MODE_VIDEOLIST || mListMode == MODE_FULL_VIDEOLIST) {
                String videoID = "";
                if (mListMode == MODE_VIDEOLIST) {
                    if (mTargetVideoListInfo == null) {
                        return;
                    }

                    videoID = mTargetVideoListInfo.get(position).getVideoID();
                } else if (mListMode == MODE_FULL_VIDEOLIST) {
                    if (mTargetFullVideoListInfo == null) {
                        return;
                    }

                    videoID = mTargetFullVideoListInfo.get(position).getVideoID();
                }
                Utils.showVideo(MoaBJDetailBJActivity.this, videoID, mTargetBJInfo.getThumnailurl());
            }
        }
    };

    private final View.OnClickListener mExtensionBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mExtensionMode == EXTENSION_DETAIL) {
                mExtensionView.setImageResource(R.drawable.info_open);
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mBJContent.getLayoutParams();
                layoutParams.topMargin = (int) (-155 * mDeviceDensity);
                mBJContent.setLayoutParams(layoutParams);
                mExtensionMode = EXTENSION_SIMPLE;
            } else if (mExtensionMode == EXTENSION_SIMPLE) {
                mExtensionView.setImageResource(R.drawable.info_fold);
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mBJContent.getLayoutParams();
                layoutParams.topMargin = 0;
                mBJContent.setLayoutParams(layoutParams);
                mExtensionMode = EXTENSION_DETAIL;
            }
        }
    };

    private final View.OnClickListener mTabListBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mLoadingView.getVisibility() == View.VISIBLE) {
                return;
            }

            if (v.equals(mTabPlayListBtn)) {
                if (mTargetBJInfo.getType().equals(NO_PLAY_LIST)) {
                    Toast.makeText(MoaBJDetailBJActivity.this, R.string.str_no_exist_playlist, Toast.LENGTH_SHORT).show();
                    return;
                }

                showLoadingView();
                mListMode = MODE_PLAYLIST;
                mTabVideoListBtn.setSelected(false);
                mTabPlayListBtn.setSelected(true);

                if (mTargetPlayListInfo != null && mTargetPlayListInfo.size() > 0) {
                    mVideosCount.setText(convertCount(getTotalVideoCount()));
                    mPlayListAdapter = new PlayListAdapter(MoaBJDetailBJActivity.this, mTargetPlayListInfo, mTargetBJInfo);
                    mPlayListView.setAdapter(mPlayListAdapter);
                    dismissLoadingView();
                } else {
                    ServerProxy.getInstance(MoaBJDetailBJActivity.this).requestTargetBJChannelList(mChannelListResponseCallback, mTargetBJInfo.getBJChannelID(), "");
                }
            } else if (v.equals(mTabVideoListBtn)) {
                showLoadingView();
                updateTitle(mTargetBJInfo.getBjName());
                mListMode = MODE_FULL_VIDEOLIST;
                mTabPlayListBtn.setSelected(false);
                mTabVideoListBtn.setSelected(true);

                if (mTargetFullVideoListInfo != null && mTargetFullVideoListInfo.size() > 0) {
                    mVideosCount.setText(convertCount(getTotalVideoCount()));
                    mBJVideoListAdapter = new VideoListAdapter(MoaBJDetailBJActivity.this, mTargetFullVideoListInfo, mTargetBJInfo);
                    mPlayListView.setAdapter(mBJVideoListAdapter);
                    dismissLoadingView();
                } else {
                    ServerProxy.getInstance(MoaBJDetailBJActivity.this).requestFullVideoList(mFullVideoListResponseCallback, mTargetBJInfo.getBJChannelID(), "");
                }
            }
        }
    };

    private final View.OnClickListener mBackBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onBackPressed();
        }
    };

    private final View.OnClickListener mSubBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.youtube_subscription_btn:
                    if (MoaBJPreferenceManager.getAccountName(MoaBJDetailBJActivity.this).equals(SIGN_OUT_STATE)) {
                        Toast.makeText(MoaBJDetailBJActivity.this, R.string.str_no_signin, Toast.LENGTH_SHORT).show();
                    } else {
                        LogUtil.LOGD("onClickListener :: mCheckSubscription = " + Integer.toString(mCheckSubscription));
                        LogUtil.LOGD("onClickListener :: getAccountName() :: " + MoaBJPreferenceManager.getAccountName(MoaBJDetailBJActivity.this));
                        mYoutubeSubscriptionBtn.setEnabled(false);
                        if (mCheckSubscription == 0) {
                            mSubscriptionOption = COMMAND_SUBSCRIPTION;
                            new SubscriptTask().execute();
                            mTracker.send(new HitBuilders.EventBuilder().setCategory("MoaBJDetailBJActivity").setAction("Subscription").setLabel("Do Subscription").build());
                        } else if (mCheckSubscription == 1) {
                            mSubscriptionOption = COMMAND_DISSUBSCRIPTION;
                            new SubscriptTask().execute();
                            mTracker.send(new HitBuilders.EventBuilder().setCategory("MoaBJDetailBJActivity").setAction("Subscription").setLabel("Do Dissubscription").build());
                        }
                    }
                    break;
                case R.id.go_afreeca_btn:
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mTargetBJInfo.getAfracaURL()));
                    startActivity(intent);
                    break;

                case R.id.favorite_btn:
                    if (mTargetBJInfo.isFavorite()) {
                        Toast.makeText(MoaBJDetailBJActivity.this, R.string.str_favorite_delete, Toast.LENGTH_SHORT).show();
                        mDataManager.deleteFavoriteBjInfo(mTargetBJInfo.getBJChannelID());
                        mFavoriteBtn.setImageResource(R.drawable.bookmark_btn_off);
                        mTargetBJInfo.setFavorite(false);
                        sendBookMarkBroadcast(BOOKMARK_ACTION_TYPE_DELETE, BOOKMARK_CONTENT_TYPE_BJ, mTargetBJInfo.getBJChannelID());
                    } else {
                        Toast.makeText(MoaBJDetailBJActivity.this, R.string.str_favorite_add, Toast.LENGTH_SHORT).show();
                        mDataManager.insertFavoriteBJ(mTargetBJInfo);
                        mFavoriteBtn.setImageResource(R.drawable.bookmark_btn_on);
                        mTargetBJInfo.setFavorite(true);
                        sendBookMarkBroadcast(BOOKMARK_ACTION_TYPE_ADD, BOOKMARK_CONTENT_TYPE_BJ, mTargetBJInfo.getBJChannelID());
                    }
                    break;
            }
        }
    };

    class SubscriptTask extends AsyncTask<Void, Void, Boolean> {

        private final String[] SCOPES = {Scopes.PROFILE, YouTubeScopes.YOUTUBE};

        public SubscriptTask() {
            mGoogleCredential = GoogleAccountCredential.usingOAuth2(MoaBJDetailBJActivity.this, Arrays.asList(SCOPES));
            mGoogleCredential.setSelectedAccountName(MoaBJPreferenceManager.getAccountName(MoaBJDetailBJActivity.this));
            mGoogleCredential.setBackOff(new ExponentialBackOff());
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            final YouTube youtube =
                    new YouTube.Builder(mTransport, mJsonFactory, mGoogleCredential).setApplicationName(getString(R.string.app_name)).build();

            ResourceId resourceId = new ResourceId();
            resourceId.setChannelId(mTargetBJInfo.getBJChannelID());
            resourceId.setKind("youtube#channel");

            SubscriptionSnippet snippet = new SubscriptionSnippet();
            snippet.setResourceId(resourceId);

            try {
                Subscription subscription = new Subscription();
                subscription.setSnippet(snippet);
                if (mSubscriptionOption.equals(COMMAND_CHECK_SUBSCRIPTION)) {
                    YouTube.Subscriptions.List subscriptionList = youtube.subscriptions().list("id,snippet,contentDetails").setForChannelId(mTargetBJInfo.getBJChannelID()).setMine(true);
                    SubscriptionListResponse responseOfSubscriptionIdForJson = subscriptionList.execute();
                    LogUtil.LOGD("mCheckSubscription :: " + responseOfSubscriptionIdForJson.toString());
                    mCheckSubscription = new ParseManager().parseSubscriptionValue(responseOfSubscriptionIdForJson.toString());
                    LogUtil.LOGD("CheckSubscriptTask() :: CheckSubscription :: " + Integer.toString(mCheckSubscription));
                    return true;
                } else if (mSubscriptionOption.equals(COMMAND_SUBSCRIPTION)) {
                    LogUtil.LOGD("before :: subscription =  " + subscription + " subscription.toString() = " + subscription.toString());
                    YouTube.Subscriptions.Insert subscriptionInsert = youtube.subscriptions().insert("snippet,contentDetails", subscription);
                    Subscription returnSubscription = subscriptionInsert.execute();
                    LogUtil.LOGD("after :: returnSubScription =  " + returnSubscription + " returnSubscription.toString() = " + returnSubscription.toString());
                    return true;
                } else if (mSubscriptionOption.equals(COMMAND_DISSUBSCRIPTION)) {
                    YouTube.Subscriptions.List subscriptionList = youtube.subscriptions().list("id,snippet,contentDetails").setForChannelId(mTargetBJInfo.getBJChannelID()).setMine(true);
                    SubscriptionListResponse responseOfSubscriptionIdForJson = subscriptionList.execute();
                    if (responseOfSubscriptionIdForJson.toString() == NO_HAS_SUBSCRIPTION) {
                        return false;
                    }
                    YouTube.Subscriptions.Delete subscriptionDelete = youtube.subscriptions().delete(new ParseManager().parseDeleteSubscriptionID(responseOfSubscriptionIdForJson.toString()));
                    subscriptionDelete.execute();
                    return true;
                }
            } catch (GoogleJsonResponseException e) {
                e.printStackTrace();
            } catch (UserRecoverableAuthIOException e) {
                e.printStackTrace();
                startActivityForResult(e.getIntent(), REQUEST_AUTHORIZATION);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }


        @Override
        protected void onPostExecute(Boolean isProcessSuccess) {
            super.onPostExecute(isProcessSuccess);
            if (mSubscriptionOption.equals(COMMAND_CHECK_SUBSCRIPTION)) {
                if (isProcessSuccess) {
                    if (mCheckSubscription == 0) {
                        mYoutubeSubscriptionBtn.setImageResource(R.drawable.icon_youtube);
                        if (mTargetBJInfo.getPartnertype().equals(TYPE_PARTNER)) {
                            partnerBJSubscriptionDialog();
                        }
                    } else if (mCheckSubscription == 1) {
                        mYoutubeSubscriptionBtn.setImageResource(R.drawable.icon_youtube_sub);
                    }
                } else {
                    mYoutubeSubscriptionBtn.setImageResource(R.drawable.icon_youtube);
                }
            } else if (mSubscriptionOption.equals(COMMAND_SUBSCRIPTION)) {
                if (isProcessSuccess) {
                    mYoutubeSubscriptionBtn.setImageResource(R.drawable.icon_youtube_sub);
                    mCheckSubscription = 1;
                    Toast.makeText(MoaBJDetailBJActivity.this, R.string.str_complete_subscription, Toast.LENGTH_SHORT).show();
                } else {
                    mYoutubeSubscriptionBtn.setImageResource(R.drawable.icon_youtube);
                    mCheckSubscription = 0;
                }
            } else if (mSubscriptionOption.equals(COMMAND_DISSUBSCRIPTION)) {
                if (!isProcessSuccess) {
                    mYoutubeSubscriptionBtn.setImageResource(R.drawable.icon_youtube_sub);
                    mCheckSubscription = 1;
                    Toast.makeText(MoaBJDetailBJActivity.this, R.string.str_fail_dissubscription, Toast.LENGTH_SHORT).show();
                } else {
                    mYoutubeSubscriptionBtn.setImageResource(R.drawable.icon_youtube);
                    mCheckSubscription = 0;
                    Toast.makeText(MoaBJDetailBJActivity.this, R.string.str_complete_dissubscription, Toast.LENGTH_SHORT).show();
                }
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mYoutubeSubscriptionBtn.setEnabled(true);
                }
            }, 10000);
        }
    }

    private void partnerBJSubscriptionDialog() {
        final AlertDialog.Builder subDialog = new AlertDialog.Builder(this);
        subDialog.setMessage(R.string.str_subscription_info).setCancelable(false).setPositiveButton(R.string.str_subscription, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mSubscriptionOption = COMMAND_SUBSCRIPTION;
                new SubscriptTask().execute();
                mTracker.send(new HitBuilders.EventBuilder().setCategory("MoaBJDetailBJActivity").setAction("Subscription").setLabel("Do Subscription").build());
                mYoutubeSubscriptionBtn.setEnabled(false);
            }
        }).setNegativeButton(android.R.string.cancel, null);
        subDialog.show();
    }

    private final ServerProxy.ResponseCallback mVideoListMoreResponseCallback = new ServerProxy.ResponseCallback() {
        @Override
        public void onResponse(String response) {
            if (!mIsFinished) {
                ArrayList<HotVideo> moreVideoData = new ParseManager().parseHotVideoList(response, mDataManager);
                for (int i = 0; i < moreVideoData.size(); i++) {
                    mTargetVideoListInfo.add(moreVideoData.get(i));
                }
                mPlayListAdapter.notifyDataSetChanged();
                mPlayListView.onLoadMoreComplete();
            }
        }

        @Override
        public void onError(int status) {
            mPlayListView.onLoadMoreComplete();
        }
    };

    private final ServerProxy.ResponseCallback mVideoListResponseCallback = new ServerProxy.ResponseCallback() {
        @Override
        public void onResponse(String response) {
            if (!mIsFinished) {
                dismissLoadingView();
                mTargetVideoListInfo = new ParseManager().parseHotVideoList(response, mDataManager);
                mBJVideoListAdapter = new VideoListAdapter(MoaBJDetailBJActivity.this, mTargetVideoListInfo, mTargetBJInfo);
                mPlayListView.setAdapter(mBJVideoListAdapter);
            }
        }

        @Override
        public void onError(int status) {

        }
    };

    private final ServerProxy.ResponseCallback mChannelListMoreResponseCallback = new ServerProxy.ResponseCallback() {
        @Override
        public void onResponse(String response) {
            if (!mIsFinished) {
                ArrayList<PlayListInfo> morePlayListData = new ParseManager().parsePlayList(response);
                for (int i = 0; i < morePlayListData.size(); i++) {
                    mTargetPlayListInfo.add(morePlayListData.get(i));
                }
                mPlayListAdapter.notifyDataSetChanged();
                mPlayListView.onLoadMoreComplete();
            }
        }

        @Override
        public void onError(int status) {
            mPlayListView.onLoadMoreComplete();
        }
    };

    private final ServerProxy.ResponseCallback mChannelListResponseCallback = new ServerProxy.ResponseCallback() {
        @Override
        public void onResponse(String response) {
            if (!mIsFinished) {
                dismissLoadingView();
                mTargetPlayListInfo = new ParseManager().parsePlayList(response);
                mPlayListAdapter = new PlayListAdapter(MoaBJDetailBJActivity.this, mTargetPlayListInfo, mTargetBJInfo);
                mPlayListView.setAdapter(mPlayListAdapter);

                String convertedSubscribeCount = convertCount(mTargetBJInfo.getbjsubscriberCount());
                mSubscriptionCount.setText(convertedSubscribeCount.equals("0") ? getString(R.string.str_closed) : convertedSubscribeCount);
                mVideosCount.setText(convertCount(getTotalVideoCount()));
                mHitsCount.setText(convertCount(mTargetBJInfo.getViewCount()));
            }
        }

        @Override
        public void onError(int status) {

        }
    };

    private final ServerProxy.ResponseCallback mFullVideoMoreListResponseCallback = new ServerProxy.ResponseCallback() {
        @Override
        public void onResponse(String response) {
            if (!mIsFinished) {
                ArrayList<HotVideo> moreVideoData = new ParseManager().parseHotVideoList(response, mDataManager);
                for (int i = 0; i < moreVideoData.size(); i++) {
                    mTargetFullVideoListInfo.add(moreVideoData.get(i));
                }
                mBJVideoListAdapter.notifyDataSetChanged();
                mPlayListView.onLoadMoreComplete();
            }
        }

        @Override
        public void onError(int status) {
            mPlayListView.onLoadMoreComplete();
        }
    };

    private final ServerProxy.ResponseCallback mFullVideoListResponseCallback = new ServerProxy.ResponseCallback() {
        @Override
        public void onResponse(String response) {
            if (!mIsFinished) {
                dismissLoadingView();
                mTargetFullVideoListInfo = new ParseManager().parseHotVideoList(response, mDataManager);
                mBJVideoListAdapter = new VideoListAdapter(MoaBJDetailBJActivity.this, mTargetFullVideoListInfo, mTargetBJInfo);
                mPlayListView.setAdapter(mBJVideoListAdapter);

                String convertedSubscribeCount = convertCount(mTargetBJInfo.getbjsubscriberCount());
                mSubscriptionCount.setText(convertedSubscribeCount.equals("0") ? getString(R.string.str_closed) : convertedSubscribeCount);
                mVideosCount.setText(convertCount(getTotalVideoCount()));
                mHitsCount.setText(convertCount(mTargetBJInfo.getViewCount()));
            }
        }

        @Override
        public void onError(int status) {

        }
    };

    private final BroadcastReceiver mBookMarkChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ACTION_BOOKMARK_CHANGED)) {
                Bundle bundle = intent.getExtras();
                LogUtil.LOGE("MoaBJDetailBJActivity :: ACTION_BOOKMARK_CHANGED");
                if (bundle != null && bundle.getString(EXTRA_BOOKMARK_CONTENT_TYPE).equals(BOOKMARK_CONTENT_TYPE_VIDEO)) {
                    if (mListMode == MODE_VIDEOLIST) {
                        LogUtil.LOGE("MoaBJDetailBJActivity :: ACTION_BOOKMARK_CHANGED :: MODE_VIDE_LIST");
                        if (mTargetFullVideoListInfo != null) {
                            for (int i = 0; i < mTargetFullVideoListInfo.size(); i++) {
                                if (mTargetFullVideoListInfo.get(i).getVideoID().equals(bundle.getString(EXTRA_BOOKMARK_ID))) {
                                    HotVideo item = mTargetFullVideoListInfo.get(i);
                                    if (bundle.getString(EXTRA_BOOKMARK_ACTION_TYPE).equals(BOOKMARK_ACTION_TYPE_ADD)) {
                                        item.setFavorite(true);
                                    } else {
                                        item.setFavorite(false);
                                    }
                                    mTargetFullVideoListInfo.set(i, item);
                                }
                            }
                        }
                    }
                }
            }
        }
    };
}
