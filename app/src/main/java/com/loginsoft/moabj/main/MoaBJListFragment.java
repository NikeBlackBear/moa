package com.loginsoft.moabj.main;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loginsoft.moabj.R;
import com.loginsoft.moabj.Util.LogUtil;
import com.loginsoft.moabj.common.CommonValues;
import com.loginsoft.moabj.custom.BJListAdapter;
import com.loginsoft.moabj.custom.LoadMoreListView;
import com.loginsoft.moabj.http.ParseManager;
import com.loginsoft.moabj.http.ServerProxy;
import com.loginsoft.moabj.model.BJInfo;

import java.util.ArrayList;


public class MoaBJListFragment extends MoaBJDataObserverFragment implements CommonValues {
    private final int START_BJ_LIST_PAGE_NUM = 1;
    private final int GET_BJ_DATA_COUNT = 10;

    private LoadMoreListView mBJListView;
    private ListView mSearchBJListView;
    private RelativeLayout mLoadingView;
    private ImageView mSearchTitleImage;
    private ImageView mLoadingImg;
    private EditText mSearchEditText;
    private ImageView mSearchButton;

    private BJListAdapter mBJListAdaper;
    private BJListAdapter mSearchBJListAdapter;
    private ArrayList<BJInfo> mBJData;
    private ArrayList<BJInfo> mSearchBJData;

    private int mCurrentPage = START_BJ_LIST_PAGE_NUM;
    private int mCurrentListType = TYPE_MAIN_LIST;

    private InputMethodManager mInputMethodManager;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        LogUtil.LOGE("MoaBJListFragment() :: onCreateView()");
        return inflater.inflate(R.layout.fragment_bj_list, null);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        LogUtil.LOGE("MoaBJListFragment() :: onActivityCreated()");
        init();

        Tracker t = ((MoaBJAnalystics) getActivity().getApplication()).getTracker(getActivity(), MoaBJAnalystics.TrackerName.APP_TRACKER);
        t.setScreenName("MoaBJListFragment");
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    void onBookMarkChanged(String bookMarkActionType, String bookMarkContentType, String id) {
        LogUtil.LOGE("MoaBJListFragment() :: onBookMarkChanged() :: bookMarkActionType = " + bookMarkActionType + " bookMarkContentType = " + bookMarkContentType + " id = " + id);
        if (bookMarkContentType.equals(BOOKMARK_CONTENT_TYPE_BJ)) {
            if (mBJData != null) {
                for (int i = 0; i < mBJData.size(); i++) {
                    BJInfo item = mBJData.get(i);
                    if (item.getBJChannelID().equals(id)) {
                        if (bookMarkActionType.equals(BOOKMARK_ACTION_TYPE_ADD)) {
                            item.setFavorite(true);
                        } else if (bookMarkActionType.equals(BOOKMARK_ACTION_TYPE_DELETE)) {
                            item.setFavorite(false);
                        }
                        mBJData.set(i, item);
                        break;
                    }
                }
                mBJListAdaper.notifyDataSetChanged();
            }

            if (mCurrentListType == TYPE_SEARCH_LIST && mSearchBJData != null) {
                for (int i = 0; i < mSearchBJData.size(); i++) {
                    BJInfo item = mSearchBJData.get(i);
                    if (item.getBJChannelID().equals(id)) {
                        if (bookMarkActionType.equals(BOOKMARK_ACTION_TYPE_ADD)) {
                            item.setFavorite(true);
                        } else if (bookMarkActionType.equals(BOOKMARK_ACTION_TYPE_DELETE)) {
                            item.setFavorite(false);
                        }
                        mSearchBJData.set(i, item);
                        break;
                    }
                }
                mSearchBJListAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    void onDisplay() {
        LogUtil.LOGE("MoaBJListFragment() :: onDisplay()");
        GoogleAnalytics.getInstance(getActivity()).reportActivityStart(getActivity());
    }

    @Override
    void onDismiss() {
        LogUtil.LOGE("MoaBJListFragment() :: onDismiss()");
        hideKeySoftKeyBoard();
        GoogleAnalytics.getInstance(getActivity()).reportActivityStop(getActivity());
    }

    private void init() {
        mInputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        View root = getView();
        mBJListView = (LoadMoreListView) root.findViewById(R.id.bjlist);
        mSearchBJListView = (ListView) root.findViewById(R.id.searchbjlist);
        mLoadingView = (RelativeLayout) root.findViewById(R.id.loading_view);
        mLoadingImg = (ImageView) root.findViewById(R.id.loadingImg);
        mSearchEditText = (EditText) root.findViewById(R.id.serch_edit_text);
        mSearchButton = (ImageView) root.findViewById(R.id.serch_img);
        mSearchTitleImage = (ImageView) root.findViewById(R.id.search_sub_btn);

        mLoadingView.setBackgroundColor(Color.WHITE);
        mLoadingImg.setBackgroundResource(R.drawable.loadinganimationblack);
        mSearchEditText.setOnEditorActionListener(mSearchActionListener);
        mBJListView.setOnItemClickListener(mBJListItemClickListener);
        mBJListView.setOnLoadMoreListener(mBJListMoreListener);
        mSearchBJListView.setOnItemClickListener(mBJListItemClickListener);
        mSearchButton.setOnClickListener(mSearchBtnListener);
        mSearchTitleImage.setOnClickListener(mSearchBtnListener);

        ServerProxy.getInstance(getActivity()).requestBJList(mBJListResponseCallback, START_BJ_LIST_PAGE_NUM, GET_BJ_DATA_COUNT);
        showLoadingView();
    }

    private void showLoadingView() {
        mLoadingView.setVisibility(View.VISIBLE);
        ((AnimationDrawable) mLoadingImg.getBackground()).start();
    }

    private void dismissLoadingView() {
        mLoadingView.setVisibility(View.GONE);
        ((AnimationDrawable) mLoadingImg.getBackground()).stop();
    }

    public int getCurrentListType() {
        return mCurrentListType;
    }

    public void backToMainBJList() {
        if (mLoadingView.getVisibility() == View.VISIBLE) {
            dismissLoadingView();
        }
        mSearchTitleImage.setImageResource(R.drawable.search_title);
        mSearchBJListView.setVisibility(View.GONE);
        mBJListView.setVisibility(View.VISIBLE);
        mCurrentListType = TYPE_MAIN_LIST;
    }

    private void hideKeySoftKeyBoard() {
        if (mInputMethodManager != null) {
            mInputMethodManager.hideSoftInputFromWindow(mSearchEditText.getWindowToken(), 0);
        }
    }

    private void doSearch() {
        if (!mSearchEditText.getText().toString().equals("")) {
            hideKeySoftKeyBoard();
            ServerProxy.getInstance(getActivity()).requestSerchBJ(mSearchBJResponseCallback, mSearchEditText.getText().toString().trim());
            showLoadingView();
            mBJListView.setVisibility(View.GONE);
            mSearchEditText.setText("");
            mCurrentListType = TYPE_SEARCH_LIST;
        }
    }


    private final TextView.OnEditorActionListener mSearchActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                doSearch();
            }
            return false;
        }
    };

    private final View.OnClickListener mSearchBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.equals(mSearchButton)) {
                doSearch();
            } else if (v.equals(mSearchTitleImage)) {
                if (mCurrentListType == TYPE_SEARCH_LIST) {
                    backToMainBJList();
                }
            }
        }
    };

    private final AdapterView.OnItemClickListener mBJListItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent intent = new Intent(getActivity(), MoaBJDetailBJActivity.class);
            if (mCurrentListType == TYPE_MAIN_LIST) {
                intent.putExtra(EXTRA_BJ_ITEM, mBJData.get(position));
            } else if (mCurrentListType == TYPE_SEARCH_LIST) {
                intent.putExtra(EXTRA_BJ_ITEM, mSearchBJData.get(position));
            }
            startActivity(intent);
        }
    };

    private final LoadMoreListView.OnLoadMoreListener mBJListMoreListener = new LoadMoreListView.OnLoadMoreListener() {
        @Override
        public void onLoadMore() {
            ServerProxy.getInstance(getActivity()).requestBJList(mBJListMoreResponseCallback, mCurrentPage + 1, GET_BJ_DATA_COUNT);
        }
    };

    private final ServerProxy.ResponseCallback mSearchBJResponseCallback = new ServerProxy.ResponseCallback() {
        @Override
        public void onResponse(String response) {
            LogUtil.LOGV("mSearchBJResponseCallback :: onResponse() :: response = " + response);
            ParseManager manager = new ParseManager();
            mSearchBJData = manager.parseBJList(response, getDataManager());
            mSearchBJListAdapter = new BJListAdapter(getActivity(), mSearchBJData);
            dismissLoadingView();
            if (mSearchBJData.size() > 0) {
                mSearchBJListView.setAdapter(mSearchBJListAdapter);
                mSearchBJListView.setVisibility(View.VISIBLE);
                mSearchTitleImage.setImageResource(R.drawable.search_back_btn);
            } else {
                backToMainBJList();
                Toast.makeText(getActivity(), R.string.str_no_search_data, Toast.LENGTH_SHORT).show();
            }
            LogUtil.LOGE("mSearchBJResponseCallback :: onResponse() :: mSearchBJData = " + mSearchBJData + " mSearchBJData.size() = " + mSearchBJData.size());
        }

        @Override
        public void onError(int status) {
            LogUtil.LOGV("mSearchBJResponseCallback :: onError() :: status = " + status);
        }
    };

    private final ServerProxy.ResponseCallback mBJListMoreResponseCallback = new ServerProxy.ResponseCallback() {
        @Override
        public void onResponse(String response) {
            LogUtil.LOGV("mBJListMoreResponseCallback :: onResponse() :: response = " + response);
            if (!response.equals("")) {
                ArrayList<BJInfo> moreBJData = new ParseManager().parseBJList(response, getDataManager());
                for (int i = 0; i < moreBJData.size(); i++) {
                    mBJData.add(moreBJData.get(i));
                }
                mBJListAdaper.notifyDataSetChanged();
                mCurrentPage += 1;
            } else {
                LogUtil.LOGD("MoaBJLIstFragment :: mBJListMoreResponseCallback :: not exists next data");
            }
            mBJListView.onLoadMoreComplete();
        }

        @Override
        public void onError(int status) {
            LogUtil.LOGV("mBJListResponseCallback :: onError() :: status = " + status);
            mBJListView.onLoadMoreComplete();
        }
    };

    private final ServerProxy.ResponseCallback mBJListResponseCallback = new ServerProxy.ResponseCallback() {
        @Override
        public void onResponse(String response) {
            if (getActivity() != null) {
                LogUtil.LOGV("mBJListResponseCallback :: onResponse() :: response = " + response);
                LogUtil.LOGE("response 1 = " + response.trim().charAt(0) + " response last = " + response.trim().charAt(response.trim().length() - 1));
                ParseManager manager = new ParseManager();
                mBJData = manager.parseBJList(response, getDataManager());
                mBJListAdaper = new BJListAdapter(getActivity(), mBJData);
                mBJListView.setAdapter(mBJListAdaper);
                dismissLoadingView();
                mBJListView.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onError(int status) {
            LogUtil.LOGV("mBJListResponseCallback :: onError() :: status = " + status);
        }
    };
}