package com.loginsoft.moabj.main;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loginsoft.moabj.R;
import com.loginsoft.moabj.Util.DateTime;
import com.loginsoft.moabj.Util.LogUtil;
import com.loginsoft.moabj.Util.Utils;
import com.loginsoft.moabj.common.CommonValues;
import com.loginsoft.moabj.custom.ImageViewer;
import com.loginsoft.moabj.model.HotVideo;

import java.text.NumberFormat;

/**
 * 메인 Fragment 의 상단 5개 탑 비디오 Viewpager 에 들어갈 Fragment
 */
public class MoaBJTopSubFragment extends Fragment implements CommonValues {

    private ImageViewer mVideoThumbnail;
    private ImageViewer mBJIcon;
    private ImageView mPlayBtn;
    private TextView mVideoPlayTime;
    private ImageView mFavoriteBtn;
    private TextView mBJTitle;
    private TextView mVideoTitle;
    private TextView mViewCount;
    private TextView mUploadDate;
    private int mIndicatorResIds[] = {R.id.indicator_1, R.id.indicator_2, R.id.indicator_3, R.id.indicator_4, R.id.indicator_5};

    private HotVideo mHotVideoItem;
    private int mPageNumber = -1;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_top_sub, null);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(mBookMarkChangedReceiver);
    }

    private void init() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_BOOKMARK_CHANGED);
        getActivity().registerReceiver(mBookMarkChangedReceiver, filter);

        View root = getView();
        mVideoThumbnail = (ImageViewer) root.findViewById(R.id.hotVideoThumbnail);
        mBJIcon = (ImageViewer) root.findViewById(R.id.bj_thumbnail);
        mPlayBtn = (ImageView) root.findViewById(R.id.play_btn);
        mVideoPlayTime = (TextView) root.findViewById(R.id.video_time);
        mBJTitle = (TextView) root.findViewById(R.id.bj_name);
        mVideoTitle = (TextView) root.findViewById(R.id.video_title);
        mViewCount = (TextView) root.findViewById(R.id.view_count);
        mUploadDate = (TextView) root.findViewById(R.id.upload_date);
        mFavoriteBtn = (ImageView) root.findViewById(R.id.bookmark_btn);
        mFavoriteBtn.setOnClickListener(mFavoriteBtnListener);
        mPlayBtn.setOnClickListener(mVideoPlayBtnListener);

        setContent();
    }

    private void setContent() {
        mHotVideoItem = (HotVideo) getArguments().getSerializable(EXTRA_HOT_VIDEO);
        if (mHotVideoItem != null) {
            mPageNumber = getArguments().getInt(EXTRA_SUB_POSITION);
            ((ImageView) getView().findViewById(mIndicatorResIds[mPageNumber])).setImageResource(R.drawable.indicator_on);
            mVideoThumbnail.setImageURL(mHotVideoItem.getVideoThumbnail());
            mBJIcon.setImageURL(mHotVideoItem.getBJIcon());
            mVideoPlayTime.setText(mHotVideoItem.getDuration());
            mBJTitle.setText(mHotVideoItem.getChannelTitle());
            mVideoTitle.setText(mHotVideoItem.getTitle());
            try {
                mViewCount.setText(NumberFormat.getInstance().format(Integer.parseInt(mHotVideoItem.getViewCount())));
            } catch (Exception e) {
                e.printStackTrace();
                mViewCount.setText("?");
            }
            mUploadDate.setText(DateTime.getUploadDateCompareCurrentDate(getActivity(), mHotVideoItem.getUploadDate()));
            mVideoTitle.setSelected(true);

            updateBookMarkState();
        }

    }

    private void updateBookMarkState() {
        if (mHotVideoItem.isFavorite()) {
            mFavoriteBtn.setImageResource(R.drawable.bookmark_btn_on);
        } else {
            mFavoriteBtn.setImageResource(R.drawable.bookmark_btn_off);
        }
    }

    private void sendBookMarkBroadcast(String bookMarkActionType, String bookMarkContent, String id) {
        Intent intent = new Intent(ACTION_BOOKMARK_CHANGED);
        intent.putExtra(EXTRA_BOOKMARK_CONTENT_TYPE, bookMarkContent);
        intent.putExtra(EXTRA_BOOKMARK_ACTION_TYPE, bookMarkActionType);
        intent.putExtra(EXTRA_BOOKMARK_ID, id);
        getActivity().sendBroadcast(intent);
    }

    private final View.OnClickListener mVideoPlayBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Utils.showVideo(getActivity(), mHotVideoItem.getVideoID(), mHotVideoItem.getBJIcon());
        }
    };

    public final View.OnClickListener mFavoriteBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mHotVideoItem.isFavorite()) {
                Toast.makeText(getActivity(), R.string.str_favorite_delete, Toast.LENGTH_SHORT).show();
                ((MoaBJMainFrame) getActivity()).getDataManager().deleteFavoriteVideoInfo(mHotVideoItem.getVideoID());
                mFavoriteBtn.setImageResource(R.drawable.bookmark_btn_off);
                mHotVideoItem.setFavorite(false);
                sendBookMarkBroadcast(BOOKMARK_ACTION_TYPE_DELETE, BOOKMARK_CONTENT_TYPE_VIDEO, mHotVideoItem.getVideoID());
            } else {
                Toast.makeText(getActivity(), R.string.str_favorite_add, Toast.LENGTH_SHORT).show();
                ((MoaBJMainFrame) getActivity()).getDataManager().insertFavoriteVideoData(mHotVideoItem);
                mFavoriteBtn.setImageResource(R.drawable.bookmark_btn_on);
                mHotVideoItem.setFavorite(true);
                sendBookMarkBroadcast(BOOKMARK_ACTION_TYPE_ADD, BOOKMARK_CONTENT_TYPE_VIDEO, mHotVideoItem.getVideoID());
            }
        }
    };

    private final BroadcastReceiver mBookMarkChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ACTION_BOOKMARK_CHANGED)) {
                Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    String bookMarkActionType = bundle.getString(EXTRA_BOOKMARK_ACTION_TYPE);
                    String bookMarkContentType = bundle.getString(EXTRA_BOOKMARK_CONTENT_TYPE);
                    String id = bundle.getString(EXTRA_BOOKMARK_ID);
                    LogUtil.LOGE("MoaBJTopSubFragment :: mBookMarkChangedReceiver :: pageNumber = " + mPageNumber + " bookMarkActionType = " + bookMarkActionType + " bookMarkContentType = " + bookMarkContentType + " id = " + id);

                    if (bookMarkContentType.equals(BOOKMARK_CONTENT_TYPE_VIDEO)) {
                        if (mHotVideoItem.getVideoID().equals(id)) {
                            if (bookMarkActionType.equals(BOOKMARK_ACTION_TYPE_ADD)) {
                                mHotVideoItem.setFavorite(true);
                            } else if (bookMarkActionType.equals(BOOKMARK_ACTION_TYPE_DELETE)) {
                                mHotVideoItem.setFavorite(false);
                            }
                            updateBookMarkState();
                        }
                    }
                }
            }
        }
    };
}