package com.loginsoft.moabj.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loginsoft.moabj.R;
import com.loginsoft.moabj.Util.LogUtil;
import com.loginsoft.moabj.Util.Utils;
import com.loginsoft.moabj.common.CommonValues;
import com.loginsoft.moabj.custom.FavoriteBJAdapter;
import com.loginsoft.moabj.custom.FavoriteVideoAdapter;
import com.loginsoft.moabj.custom.OnDeleteBookMarkListener;
import com.loginsoft.moabj.model.BJInfo;
import com.loginsoft.moabj.model.HotVideo;

import java.util.List;

/**
 * Created by Administrator on 2015-02-02.
 */
public class MoaBJBookMarkFragment extends MoaBJDataObserverFragment implements CommonValues {
    public GridView mBJBookMarkGridView;
    public ListView mVideoBookMarkListView;
    public ImageView mNoDataImageView;
    public ImageView mBJBookMarkBtn;
    public ImageView mVideoBookMarkBtn;
    private FavoriteBJAdapter mBJBookMarkAdapter;
    private FavoriteVideoAdapter mVideoBookMarkAdapter;
    private List<BJInfo> mBJBookMarkData;
    private List<HotVideo> mVideoBookMarkData;

    private String mBookMarkViewType = BOOKMARK_CONTENT_TYPE_BJ;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        LogUtil.LOGE("MoaBJBookMarkFragment() :: onCreateView()");
        return inflater.inflate(R.layout.fragment_bookmark, null);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        LogUtil.LOGE("MoaBJBookMarkFragment() :: onActivityCreated()");
        init();

        Tracker t = ((MoaBJAnalystics) getActivity().getApplication()).getTracker(getActivity(), MoaBJAnalystics.TrackerName.APP_TRACKER);
        t.setScreenName("MoaBJBookMarkFragment");
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    void onBookMarkChanged(String bookMarkActionType, String bookMarkContentType, String id) {
        LogUtil.LOGE("MoaBJBookMarkFragment() :: onBookMarkChanged() :: bookMarkActionType = " + bookMarkActionType + " bookMarkContentType = " + bookMarkContentType + " id = " + id);
        if (bookMarkContentType.equals(BOOKMARK_CONTENT_TYPE_BJ)) {
            if (bookMarkActionType.equals(BOOKMARK_ACTION_TYPE_ADD)) {
                mBJBookMarkData.add(getDataManager().getTargetFavoriteBJData(id));
                if (mBookMarkViewType == BOOKMARK_CONTENT_TYPE_BJ && mBJBookMarkData.size() == 1) {
                    mNoDataImageView.setVisibility(View.GONE);
                    mBJBookMarkGridView.setVisibility(View.VISIBLE);
                }
            } else if (bookMarkActionType.equals(BOOKMARK_ACTION_TYPE_DELETE)) {
                for (int i = 0; i < mBJBookMarkData.size(); i++) {
                    if (mBJBookMarkData.get(i).getBJChannelID().equals(id)) {
                        mBJBookMarkData.remove(i);
                        break;
                    }
                }
                if (mBookMarkViewType == BOOKMARK_CONTENT_TYPE_BJ && mBJBookMarkData.size() == 0) {
                    mNoDataImageView.setVisibility(View.VISIBLE);
                    mBJBookMarkGridView.setVisibility(View.GONE);
                }
            }
            mBJBookMarkAdapter.notifyDataSetChanged();
        } else if (bookMarkContentType.equals(BOOKMARK_CONTENT_TYPE_VIDEO)) {
            if (bookMarkActionType.equals(BOOKMARK_ACTION_TYPE_ADD)) {
                mVideoBookMarkData.add(getDataManager().getTargetFavoriteVideoData(id));
                if (mBookMarkViewType == BOOKMARK_CONTENT_TYPE_VIDEO && mVideoBookMarkData.size() == 1) {
                    mNoDataImageView.setVisibility(View.GONE);
                    mVideoBookMarkListView.setVisibility(View.VISIBLE);
                }
            } else if (bookMarkActionType.equals(BOOKMARK_ACTION_TYPE_DELETE)) {
                for (int i = 0; i < mVideoBookMarkData.size(); i++) {
                    if (mVideoBookMarkData.get(i).getVideoID().equals(id)) {
                        mVideoBookMarkData.remove(i);
                        break;
                    }
                }
                if (mBookMarkViewType == BOOKMARK_CONTENT_TYPE_VIDEO && mVideoBookMarkData.size() == 0) {
                    mNoDataImageView.setVisibility(View.VISIBLE);
                    mVideoBookMarkListView.setVisibility(View.GONE);
                }
            }
            mVideoBookMarkAdapter.notifyDataSetChanged();
        }
    }

    @Override
    void onDisplay() {
        LogUtil.LOGE("MoaBJBookMarkFragment() :: onDisplay()");
        GoogleAnalytics.getInstance(getActivity()).reportActivityStart(getActivity());
    }

    @Override
    void onDismiss() {
        LogUtil.LOGE("MoaBJBookMarkFragment() :: onDismiss()");
        GoogleAnalytics.getInstance(getActivity()).reportActivityStop(getActivity());
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void init() {
        View root = getView();

        mBJBookMarkGridView = (GridView) root.findViewById(R.id.gridView);
        mVideoBookMarkListView = (ListView) root.findViewById(R.id.listView);
        mNoDataImageView = (ImageView) root.findViewById(R.id.nofavoritedate);
        mBJBookMarkBtn = (ImageView) root.findViewById(R.id.bjon);
        mVideoBookMarkBtn = (ImageView) root.findViewById(R.id.videoon);

        if (getDataManager() != null) {
            if (getDataManager().isExistBJFavoriteData()) {
                mNoDataImageView.setVisibility(View.GONE);
            } else {
                mBJBookMarkGridView.setVisibility(View.GONE);
                mVideoBookMarkListView.setVisibility(View.GONE);
                mNoDataImageView.setVisibility(View.VISIBLE);
            }
        }

        mVideoBookMarkData = getDataManager().getAllFavoriteVideoData();
        mBJBookMarkData = getDataManager().getAllFavoriteBJData();

        mBJBookMarkAdapter = new FavoriteBJAdapter(getActivity(), mBJBookMarkData);
        mBJBookMarkAdapter.setOnDeleteBookMarkListener(mDeleteBookMarkListener);
        mVideoBookMarkAdapter = new FavoriteVideoAdapter(getActivity(), mVideoBookMarkData);
        mVideoBookMarkAdapter.setOnDeleteBookMarkListener(mDeleteBookMarkListener);

        mBJBookMarkGridView.setAdapter(mBJBookMarkAdapter);
        mVideoBookMarkListView.setAdapter(mVideoBookMarkAdapter);

        mBJBookMarkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bjBtnClick();
            }
        });
        mVideoBookMarkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoBtnClick();
            }
        });
        mVideoBookMarkListView.setOnItemClickListener(mFavoriteVideoClickListener);
        mBJBookMarkGridView.setOnItemClickListener(mFavoriteBJClickListener);
    }

    private void bjBtnClick() {
        mBookMarkViewType = BOOKMARK_CONTENT_TYPE_BJ;

        mVideoBookMarkListView.setVisibility(View.GONE);
        mBJBookMarkBtn.setImageResource(R.drawable.bjon);
        mVideoBookMarkBtn.setImageResource(R.drawable.videooff);

        if (getDataManager().isExistBJFavoriteData()) {
            mNoDataImageView.setVisibility(View.GONE);
            mBJBookMarkGridView.setVisibility(View.VISIBLE);
        } else {
            mNoDataImageView.setVisibility(View.VISIBLE);
            mBJBookMarkGridView.setVisibility(View.GONE);
        }
    }

    private void videoBtnClick() {
        mBookMarkViewType = BOOKMARK_CONTENT_TYPE_VIDEO;

        mBJBookMarkGridView.setVisibility(View.GONE);
        mBJBookMarkBtn.setImageResource(R.drawable.bjoff);
        mVideoBookMarkBtn.setImageResource(R.drawable.videoon);

        if (getDataManager().isExistVideoFavoriteData()) {
            mNoDataImageView.setVisibility(View.GONE);
            mVideoBookMarkListView.setVisibility(View.VISIBLE);
        } else {
            mNoDataImageView.setVisibility(View.VISIBLE);
            mVideoBookMarkListView.setVisibility(View.GONE);
        }
    }

    private final AdapterView.OnItemClickListener mFavoriteBJClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            LogUtil.LOGD("MoaBJMainFragment :: onItemClick() :: position = " + position);
            Intent intent = new Intent(getActivity(), MoaBJDetailBJActivity.class);
            intent.putExtra(EXTRA_BJ_ITEM, mBJBookMarkData.get(position));
            startActivity(intent);
        }
    };

    private final AdapterView.OnItemClickListener mFavoriteVideoClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Utils.showVideo(getActivity(), mVideoBookMarkData.get(position).getVideoID(), mVideoBookMarkData.get(position).getBJIcon());
        }
    };

    private final OnDeleteBookMarkListener mDeleteBookMarkListener = new OnDeleteBookMarkListener() {
        @Override
        public void onDelete(int position, Object deleteItem) {
            Intent bookMarkChangedIntent = new Intent(ACTION_BOOKMARK_CHANGED);
            if (deleteItem instanceof BJInfo) {
                mBJBookMarkData.remove(deleteItem);
                mBJBookMarkAdapter.notifyDataSetChanged();
                bookMarkChangedIntent.putExtra(EXTRA_BOOKMARK_CONTENT_TYPE, BOOKMARK_CONTENT_TYPE_BJ);
                bookMarkChangedIntent.putExtra(EXTRA_BOOKMARK_ID, ((BJInfo) deleteItem).getBJChannelID());

                if (mBJBookMarkData.size() == 0) {
                    mNoDataImageView.setVisibility(View.VISIBLE);
                    mBJBookMarkGridView.setVisibility(View.GONE);
                }
            } else if (deleteItem instanceof HotVideo) {
                mVideoBookMarkData.remove(deleteItem);
                mVideoBookMarkAdapter.notifyDataSetChanged();
                bookMarkChangedIntent.putExtra(EXTRA_BOOKMARK_CONTENT_TYPE, BOOKMARK_CONTENT_TYPE_VIDEO);
                bookMarkChangedIntent.putExtra(EXTRA_BOOKMARK_ID, ((HotVideo) deleteItem).getVideoID());

                if (mVideoBookMarkData.size() == 0) {
                    mNoDataImageView.setVisibility(View.VISIBLE);
                    mVideoBookMarkListView.setVisibility(View.GONE);
                }
            }
            bookMarkChangedIntent.putExtra(EXTRA_BOOKMARK_ACTION_TYPE, BOOKMARK_ACTION_TYPE_DELETE);
            getActivity().sendBroadcast(bookMarkChangedIntent);
        }
    };
}