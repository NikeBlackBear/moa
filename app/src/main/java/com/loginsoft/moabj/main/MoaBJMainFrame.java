package com.loginsoft.moabj.main;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loginsoft.moabj.R;
import com.loginsoft.moabj.Util.LogUtil;
import com.loginsoft.moabj.Util.Utils;
import com.loginsoft.moabj.common.CommonValues;
import com.loginsoft.moabj.custom.MainPagerAdapter;
import com.loginsoft.moabj.custom.MoaBJMainTabView;
import com.loginsoft.moabj.custom.ZoomOutPageTransformer;
import com.loginsoft.moabj.database.DataManager;
import com.loginsoft.moabj.http.ParseManager;
import com.loginsoft.moabj.http.ServerProxy;
import com.loginsoft.moabj.service.MoaService;


public class MoaBJMainFrame extends ActionBarActivity implements CommonValues {

    private final int INTRO_IMAGE_DISPLAY_DELAY = 3000;

    private final int TYPE_COMPULSION_UPDATE = 100; //업데이트를 반드시 받아야 될때
    private final int TYPE_SELECTIVE_UPDATE = 101; //업데이트 버전이 존재하지만 무조건 받지 않아도 될때
    private final int TYPE_NOT_NEED_UPDATE = 102; //업데이트가 필요하지 않을때

    private RelativeLayout mContent;
    private ImageView mIntroImg;
    private MoaBJMainTabView mTabView;
    private ViewPager mMainViewPager;
    private MainPagerAdapter mMainPagerAdapter;
    private AdView mAdView;
    private InterstitialAd mInterstitialAd;

    private DataManager mDataManager;

    private int mSelectedFragmentIdx = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_BOOKMARK_CHANGED);
        registerReceiver(mBookMarkChangedReceiver, filter);

        setContentView(R.layout.activity_main_frame);
        mIntroImg = (ImageView) findViewById(R.id.intro);
        mIntroImg.setBackgroundResource(R.drawable.loadinganimation);
        AnimationDrawable anim = (AnimationDrawable) mIntroImg.getBackground();
        anim.start();

        if (!Utils.isNetworkConnected(MoaBJMainFrame.this)) {
            showCheckNetworkStateDialog();
        } else {
            ServerProxy.getInstance(getApplicationContext()).requestNotice(mAppNoticeResponsCallback);
        }
    }

    private void init() {
        mDataManager = new DataManager(this);

        startMoaService();
        mContent = (RelativeLayout) findViewById(R.id.content);
        mMainViewPager = (ViewPager) findViewById(R.id.mainPager);
        mTabView = (MoaBJMainTabView) findViewById(R.id.frame_btn_section);
        mMainViewPager.setOnPageChangeListener(mMainPageChangeListener);
        mTabView.setMoaBJMainTabListener(mTabListener);
        mMainViewPager.setOffscreenPageLimit(5);
        mMainViewPager.setPageTransformer(false, new ZoomOutPageTransformer());
        mMainPagerAdapter = new MainPagerAdapter(getSupportFragmentManager());

        mMainViewPager.setAdapter(mMainPagerAdapter);
        showContent();
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getResources().getString(R.string.banner_ad_full_screen_id));
        mInterstitialAd.loadAd(adRequest);

        Tracker t = ((MoaBJAnalystics) getApplication()).getTracker(this, MoaBJAnalystics.TrackerName.APP_TRACKER);
        t.setScreenName("MoaBJMainFrame");
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }


    private void showContent() {
        Handler showContentHandler = new Handler();
        showContentHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mIntroImg.setVisibility(View.GONE);
                mContent.setVisibility(View.VISIBLE);
            }
        }, INTRO_IMAGE_DISPLAY_DELAY);
    }

    private void showCheckNetworkStateDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.str_alert);
        builder.setMessage(R.string.str_network_noti_msg);
        builder.setPositiveButton(android.R.string.ok, mNetworkDialogBtnListener);
        builder.create().show();
    }

    private void showFinishAppDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.str_alert);
        builder.setMessage(R.string.str_finish_app_msg);
        builder.setPositiveButton(android.R.string.ok, mFinishAppBtnListener);
        builder.setNegativeButton(android.R.string.cancel, null);
        builder.create().show();
    }

    private void showNotiDialog(String notiMsg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.str_noti_title);
        builder.setMessage(notiMsg);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                ServerProxy.getInstance(getApplicationContext()).requestAppVersion(mAppVersionResponsCallback);
            }
        });
        builder.create().show();
    }

    private void showMarket() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("market://details?id=com.loginsoft.moabj"));
        startActivity(intent);
    }

    private void showVersionUpdateDialog(int updateVersionType) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.str_appversion_check);
        if (updateVersionType == TYPE_COMPULSION_UPDATE) {
            builder.setMessage(R.string.str_launch_impossible);
            builder.setNegativeButton(R.string.str_finish, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    finish();
                }
            });
        } else if (updateVersionType == TYPE_SELECTIVE_UPDATE) {
            builder.setMessage(R.string.str_not_current_version);
            builder.setNegativeButton(R.string.str_launch_app, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    init();
                }
            });
        }

        builder.setPositiveButton(R.string.str_do_update, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                showMarket();
            }
        });

        builder.create().show();
    }

    private void startMoaService() {
        Intent intent = new Intent(this, MoaService.class);
        startService(intent);
    }

    private int versionCheck(String versionFromServer, String versionFromPhone) {
        String[] versionfromPhone = versionFromPhone.split("[.]");
        String[] versionfromServer = versionFromServer.split("[.]");

        LogUtil.LOGE("MoaBJMAinFrame :: appVersionChecker :: mAppVersionDataFromPhone =" + versionFromPhone);
        LogUtil.LOGE("MoaBJMAinFrame :: appVersionChecker :: mAppVersionDataFromServer =" + versionFromServer);

        LogUtil.LOGE("MoaBJMAinFrame :: appVersionChecker :: mAppVersionDataFromPhone =" + versionfromPhone[0] + "//" + versionfromPhone[1] + "//" + versionfromPhone[2]);
        LogUtil.LOGE("MoaBJMAinFrame :: appVersionChecker :: AppVersionFromServer =" + versionfromServer[0] + "//" + versionfromServer[1] + "//" + versionfromServer[2]);

        //버전의 첫번째 자리 혹은 두번째 자리가 다를 경우.
        if (Integer.parseInt(versionfromPhone[0]) < Integer.parseInt(versionfromServer[0]) || Integer.parseInt(versionfromPhone[1]) < Integer.parseInt(versionfromServer[1])) {
            return TYPE_COMPULSION_UPDATE;
        } else if (Integer.parseInt(versionfromPhone[2]) < Integer.parseInt(versionfromServer[2])) {
            return TYPE_SELECTIVE_UPDATE;
        } else {
            return TYPE_NOT_NEED_UPDATE;
        }
    }

    public DataManager getDataManager() {
        return mDataManager;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mBookMarkChangedReceiver);

        if (mDataManager != null) {
            mDataManager.close();
        }
    }

    @Override
    public void onBackPressed() {
        if (mSelectedFragmentIdx == FRAGMENT_BJ_LIST) {
            MoaBJListFragment bjListFragment = (MoaBJListFragment) mMainPagerAdapter.getItem(FRAGMENT_BJ_LIST);
            LogUtil.LOGE("MoaBJMainFrame :: onBackPressed() :: mSelectedFragmentIdx = " + mSelectedFragmentIdx + " listType = " + bjListFragment.getCurrentListType());
            if (bjListFragment.getCurrentListType() == TYPE_SEARCH_LIST) {
                bjListFragment.backToMainBJList();
                return;
            }
        }
        showFinishAppDialog();
    }

    private final ViewPager.OnPageChangeListener mMainPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            mTabView.setSelectTab(position);
            if (mSelectedFragmentIdx != -1 && mSelectedFragmentIdx != position) {
                ((MoaBJDataObserverFragment) mMainPagerAdapter.getItem(position)).onDisplay();
                ((MoaBJDataObserverFragment) mMainPagerAdapter.getItem(mSelectedFragmentIdx)).onDismiss();
            }
            mSelectedFragmentIdx = position;
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    private final DialogInterface.OnClickListener mNetworkDialogBtnListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            finish();
        }
    };

    private final DialogInterface.OnClickListener mFinishAppBtnListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            if (mInterstitialAd != null) {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                }
            }
            finish();
        }
    };

    private final MoaBJMainTabView.MoaBJMainTabListener mTabListener = new MoaBJMainTabView.MoaBJMainTabListener() {
        @Override
        public void onTabSelected(int position) {
            LogUtil.LOGD("MoaBJMainFrame :: onTabSelected() = " + position);
            mMainViewPager.setCurrentItem(position);
        }
    };

    private final BroadcastReceiver mBookMarkChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ACTION_BOOKMARK_CHANGED)) {
                Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    for (int i = 0; i < mMainPagerAdapter.getCount(); i++) {
                        ((MoaBJDataObserverFragment) mMainPagerAdapter.getItem(i)).onBookMarkChanged(bundle.getString(EXTRA_BOOKMARK_ACTION_TYPE), bundle.getString(EXTRA_BOOKMARK_CONTENT_TYPE), bundle.getString(EXTRA_BOOKMARK_ID));
                    }
                }
            }
        }
    };

    //서버 버전
    private final ServerProxy.ResponseCallback mAppVersionResponsCallback = new ServerProxy.ResponseCallback() {
        @Override
        public void onResponse(String response) {
            int updateVersionType = versionCheck(new ParseManager().parseAppVersion(response), Utils.getVersion(MoaBJMainFrame.this));
            if (updateVersionType == TYPE_NOT_NEED_UPDATE) {
                init();
                Toast.makeText(getApplicationContext(), "최신 버전 입니다.", Toast.LENGTH_SHORT).show();
            } else {
                showVersionUpdateDialog(updateVersionType);
            }
        }

        @Override
        public void onError(int status) {

        }
    };

    //공지사항
    private final ServerProxy.ResponseCallback mAppNoticeResponsCallback = new ServerProxy.ResponseCallback() {
        @Override
        public void onResponse(String response) {
            if (!response.equals(VALUE_NO_NOTICE)) {
                LogUtil.LOGE("MoaBJMainFrame :: notice = " + response);
                showNotiDialog(response);
            } else {
                ServerProxy.getInstance(getApplicationContext()).requestAppVersion(mAppVersionResponsCallback);
            }
        }

        @Override
        public void onError(int status) {

        }
    };

}