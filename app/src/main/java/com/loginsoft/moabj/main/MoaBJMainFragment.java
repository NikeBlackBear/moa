package com.loginsoft.moabj.main;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loginsoft.moabj.R;
import com.loginsoft.moabj.Util.LogUtil;
import com.loginsoft.moabj.common.CommonValues;
import com.loginsoft.moabj.custom.BJListAdapter;
import com.loginsoft.moabj.custom.InfinitePagerAdapter;
import com.loginsoft.moabj.custom.InfiniteViewPager;
import com.loginsoft.moabj.custom.PartnerBJAlertDialog;
import com.loginsoft.moabj.custom.SubPagerAdapter;
import com.loginsoft.moabj.custom.ZoomOutPageTransformer;
import com.loginsoft.moabj.http.ParseManager;
import com.loginsoft.moabj.http.ServerProxy;
import com.loginsoft.moabj.model.BJInfo;
import com.loginsoft.moabj.model.HotVideo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Administrator on 2015-02-02.
 */
public class MoaBJMainFragment extends MoaBJDataObserverFragment implements CommonValues {

    private final int INIT_HOT_VIDEO_COUNT = 5;
    private final int INIT_BJ_PAGE = 1;
    private final int INIT_BJ_COUNT = 100;

    private final int AUTO_PAGING_START_DELAY = 5000;
    private final int AUTO_PAGING_PERIOD = 5000;
    private final int MSG_AUTO_PAGING = 100;

    private InfiniteViewPager mSubInfinitePager;
    private InfinitePagerAdapter mSubPagerAdapter;
    private SubPagerAdapter mRealSubPagerAdapter;
    private ListView mHotBJList;
    private BJListAdapter mHotBJListAdapter;
    private Timer mAutoPagingTimer;

    private ArrayList<HotVideo> mHotVideoData;
    private ArrayList<BJInfo> mBJData;
    private PartnerBJAlertDialog mPartnerBJAlerDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        LogUtil.LOGE("MoaBJMainFragment() :: onCreateView()");
        return inflater.inflate(R.layout.fragment_main, null);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        LogUtil.LOGE("MoaBJMainFragment() :: onActivityCreated()");
        init();

        Tracker t = ((MoaBJAnalystics) getActivity().getApplication()).getTracker(getActivity(), MoaBJAnalystics.TrackerName.APP_TRACKER);
        t.setScreenName("MoaBJMainFragment");
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    void onBookMarkChanged(String bookMarkActionType, String bookMarkContentType, String id) {
        LogUtil.LOGE("MoaBJMainFragment() :: onBookMarkChanged() :: bookMarkActionType = " + bookMarkActionType + " bookMarkContentType = " + bookMarkContentType + " id = " + id);
        if (bookMarkContentType.equals(BOOKMARK_CONTENT_TYPE_BJ)) {
            if (mBJData != null) {
                for (int i = 0; i < mBJData.size(); i++) {
                    BJInfo item = mBJData.get(i);
                    if (item.getBJChannelID().equals(id)) {
                        if (bookMarkActionType.equals(BOOKMARK_ACTION_TYPE_ADD)) {
                            item.setFavorite(true);
                        } else if (bookMarkActionType.equals(BOOKMARK_ACTION_TYPE_DELETE)) {
                            item.setFavorite(false);
                        }
                        mBJData.set(i, item);
                        break;
                    }
                }
                mHotBJListAdapter.notifyDataSetChanged();
            }
        } else if (bookMarkContentType.equals(BOOKMARK_CONTENT_TYPE_VIDEO)) {
            if (mHotVideoData != null) {
                for (int i = 0; i < mHotVideoData.size(); i++) {
                    HotVideo item = mHotVideoData.get(i);
                    if (item.getVideoID().equals(id)) {
                        if (bookMarkActionType.equals(BOOKMARK_ACTION_TYPE_ADD)) {
                            item.setFavorite(true);
                        } else if (bookMarkActionType.equals(BOOKMARK_ACTION_TYPE_DELETE)) {
                            item.setFavorite(false);
                        }
                        mHotVideoData.set(i, item);
                        break;
                    }
                }
            }
        }
    }

    @Override
    void onDisplay() {
        LogUtil.LOGE("MoaBJMainFragment() :: onDisplay()");
        GoogleAnalytics.getInstance(getActivity()).reportActivityStart(getActivity());
    }

    @Override
    void onDismiss() {
        LogUtil.LOGE("MoaBJMainFragment() :: onDismiss()");
        GoogleAnalytics.getInstance(getActivity()).reportActivityStop(getActivity());
    }

    private void init() {
        View root = getView();
        mSubInfinitePager = (InfiniteViewPager) root.findViewById(R.id.subPager);
        mHotBJList = (ListView) root.findViewById(R.id.hotBJList);
        mHotBJList.setOnItemClickListener(mBJListClickListener);
        mSubInfinitePager.setOnTouchListener(mPagerTouchListener);
        mSubInfinitePager.setPageTransformer(false, new ZoomOutPageTransformer());
        ServerProxy.getInstance(getActivity()).requestHotVideoList(mHotVIdeoResponseCallback, INIT_HOT_VIDEO_COUNT);
        ServerProxy.getInstance(getActivity()).requestBJList(mBJListResponseCallback, INIT_BJ_PAGE, INIT_BJ_COUNT, TYPE_PARTNER);
    }

    @Override
    public void onResume() {
        super.onResume();
        startAutoPaging();
    }

    @Override
    public void onPause() {
        super.onPause();
        LogUtil.LOGD("MoaBJMainFragment :: onPause()");
        stopAutoPaging();
    }

    private void startAutoPaging() {
        if (mSubPagerAdapter != null && mSubInfinitePager.getVisibility() == View.VISIBLE) {
            mAutoPagingTimer = new Timer();
            mAutoPagingTimer.schedule(new AutoPagingTask(), AUTO_PAGING_START_DELAY, AUTO_PAGING_PERIOD);
        }
    }

    private void stopAutoPaging() {
        if (mAutoPagingTimer != null) {
            mAutoPagingTimer.cancel();
            mAutoPagingTimer.purge();
            if (mAutoPagingHandler.hasMessages(MSG_AUTO_PAGING)) {
                mAutoPagingHandler.removeMessages(MSG_AUTO_PAGING);
            }
            mAutoPagingTimer = null;
        }
    }

    private final AdapterView.OnItemClickListener mBJListClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            LogUtil.LOGD("MoaBJMainFragment :: onItemClick() :: position = " + position);
            Intent intent = new Intent(getActivity(), MoaBJDetailBJActivity.class);
            intent.putExtra(EXTRA_BJ_ITEM, mBJData.get(position));
            startActivity(intent);
        }
    };

    public final ServerProxy.ResponseCallback mBJListResponseCallback = new ServerProxy.ResponseCallback() {
        @Override
        public void onResponse(String response) {
            if (getActivity() != null) {
                LogUtil.LOGV("mBJListResponseCallback :: onResponse() :: response = " + response);
                mBJData = new ParseManager().parseBJList(response, getDataManager());
                Collections.shuffle(mBJData);
                mHotBJListAdapter = new BJListAdapter(getActivity(), mBJData);
                mHotBJList.setAdapter(mHotBJListAdapter);
                mPartnerBJAlerDialog = new PartnerBJAlertDialog(getActivity(), mBJData.get(new Random(System.currentTimeMillis()).nextInt(mBJData.size())));
                mPartnerBJAlerDialog.show();
            }
        }

        @Override
        public void onError(int status) {
            LogUtil.LOGV("mBJListResponseCallback :: onError() :: status = " + status);
        }
    };

    private final ServerProxy.ResponseCallback mHotVIdeoResponseCallback = new ServerProxy.ResponseCallback() {
        @Override
        public void onResponse(String response) {
            if (getActivity() != null) {
                LogUtil.LOGV("mHotVIdeoResponseCallback :: onResponse() :: response = " + response);
                mHotVideoData = new ParseManager().parseHotVideoList(response, getDataManager());
                mRealSubPagerAdapter = new SubPagerAdapter(getActivity().getSupportFragmentManager(), mHotVideoData);
                mSubPagerAdapter = new InfinitePagerAdapter(mRealSubPagerAdapter);
                mSubInfinitePager.setAdapter(mSubPagerAdapter);
                mSubInfinitePager.setVisibility(View.VISIBLE);
                startAutoPaging();
            }
        }

        @Override
        public void onError(int status) {
            LogUtil.LOGV("mHotVIdeoResponseCallback :: onError() :: status = " + status);
        }
    };

    private final View.OnTouchListener mPagerTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                startAutoPaging();
            } else {
                stopAutoPaging();
            }
            return false;
        }
    };

    private final Handler mAutoPagingHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == MSG_AUTO_PAGING) {
                mSubInfinitePager.setNextItem();
            }
        }
    };

    class AutoPagingTask extends TimerTask {
        @Override
        public void run() {
            mAutoPagingHandler.sendEmptyMessage(MSG_AUTO_PAGING);
        }
    }
}