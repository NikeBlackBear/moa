package com.loginsoft.moabj.main;

import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loginsoft.moabj.R;
import com.loginsoft.moabj.Util.LogUtil;
import com.loginsoft.moabj.Util.Utils;
import com.loginsoft.moabj.custom.TopTwentyListAdapter;
import com.loginsoft.moabj.http.ParseManager;
import com.loginsoft.moabj.http.ServerProxy;
import com.loginsoft.moabj.model.HotVideo;

import java.util.ArrayList;


/**
 * Created by Administrator on 2015-02-02.
 */
public class MoaBJTopTwentyFragment extends MoaBJDataObserverFragment {
    public ListView mTopTwentyList;
    private TopTwentyListAdapter mTopTwentyListAdaper;
    private ArrayList<HotVideo> mTopTwentyData;
    private RelativeLayout mLoadingView;
    private ImageView mLoadingImg;
    private AdView mAdView;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        LogUtil.LOGE("MoaBJTopTwentyFragment() :: onCreateView()");
        return inflater.inflate(R.layout.fragment_top_twenty, null);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        LogUtil.LOGE("MoaBJTopTwentyFragment() :: onActivityCreated()");
        init();

        Tracker t = ((MoaBJAnalystics) getActivity().getApplication()).getTracker(getActivity(), MoaBJAnalystics.TrackerName.APP_TRACKER);
        t.setScreenName("MoaBJTopTwentyFragment");
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    void onBookMarkChanged(String bookMarkActionType, String bookMarkContentType, String id) {
        LogUtil.LOGE("MoaBJTopTwentyFragment() :: onBookMarkChanged() :: bookMarkActionType = " + bookMarkActionType + " bookMarkContentType = " + bookMarkContentType + " id = " + id);
        if (bookMarkContentType.equals(BOOKMARK_CONTENT_TYPE_VIDEO)) {
            if (mTopTwentyData != null) {
                for (int i = 0; i < mTopTwentyData.size(); i++) {
                    HotVideo item = mTopTwentyData.get(i);
                    if (item.getVideoID().equals(id)) {
                        if (bookMarkActionType.equals(BOOKMARK_ACTION_TYPE_ADD)) {
                            item.setFavorite(true);
                        } else if (bookMarkActionType.equals(BOOKMARK_ACTION_TYPE_DELETE)) {
                            item.setFavorite(false);
                        }
                        mTopTwentyData.set(i, item);
                        break;
                    }
                }
                mTopTwentyListAdaper.notifyDataSetChanged();
            }
        }
    }

    @Override
    void onDisplay() {
        LogUtil.LOGE("MoaBJTopTwentyFragment() :: onDisplay()");
        GoogleAnalytics.getInstance(getActivity()).reportActivityStart(getActivity());
    }

    @Override
    void onDismiss() {
        LogUtil.LOGE("MoaBJTopTwentyFragment() :: onDismiss()");
        GoogleAnalytics.getInstance(getActivity()).reportActivityStop(getActivity());
    }

    private void init() {
        View root = getView();
        mTopTwentyList = (ListView) root.findViewById(R.id.top20list);
        mLoadingView = (RelativeLayout) root.findViewById(R.id.loading_view);
        mLoadingImg = (ImageView) root.findViewById(R.id.loadingImg);
        mTopTwentyList.setOnItemClickListener(mTopTwentyListItemClickListener);
        mLoadingView.setBackgroundColor(Color.WHITE);
        mLoadingImg.setBackgroundResource(R.drawable.loadinganimationblack);
        ServerProxy.getInstance(getActivity()).requestHotVideoList(mHotVideoResponseCallback, 20);
        showLoadingView();
    }

    private void showLoadingView() {
        mTopTwentyList.setVisibility(View.GONE);
        mLoadingView.setVisibility(View.VISIBLE);
        ((AnimationDrawable) mLoadingImg.getBackground()).start();
    }

    private void dismissLoadingView() {
        mLoadingView.setVisibility(View.GONE);
        mTopTwentyList.setVisibility(View.VISIBLE);
        ((AnimationDrawable) mLoadingImg.getBackground()).stop();
    }

    private final AdapterView.OnItemClickListener mTopTwentyListItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Utils.showVideo(getActivity(), mTopTwentyData.get(position).getVideoID(), mTopTwentyData.get(position).getBJIcon());
        }
    };

    private final ServerProxy.ResponseCallback mHotVideoResponseCallback = new ServerProxy.ResponseCallback() {
        @Override
        public void onResponse(String response) {
            if (getActivity() != null) {
                ParseManager manager = new ParseManager();
                mTopTwentyData = manager.parseHotVideoList(response, getDataManager());
                mTopTwentyListAdaper = new TopTwentyListAdapter(getActivity(), mTopTwentyData);
                mTopTwentyList.setAdapter(mTopTwentyListAdaper);
                LogUtil.LOGE("MoaBJTopTwentyFragment :: mHotVideoResponseCallback :: mTopTwentyData = " + mTopTwentyData);
                dismissLoadingView();
            }
        }

        @Override
        public void onError(int status) {

        }
    };
}