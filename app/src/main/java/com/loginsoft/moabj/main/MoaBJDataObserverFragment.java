package com.loginsoft.moabj.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.loginsoft.moabj.common.CommonValues;
import com.loginsoft.moabj.database.DataManager;

/**
 * Created by Administrator on 2015-02-23.
 */
public abstract class MoaBJDataObserverFragment extends Fragment implements CommonValues {
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public DataManager getDataManager() {
        MoaBJMainFrame activity = ((MoaBJMainFrame) getActivity());
        if (activity != null && activity.getDataManager() != null) {
            return activity.getDataManager();
        } else {
            return new DataManager(getActivity());
        }
    }

    //BookMark 화면에서 즐겨찾기 해제 시 다른 Fragment 에서 새로고침을 위한 abstract method
    abstract void onBookMarkChanged(String bookMarkActionType, String bookMarkContentType, String id);

    //현재 Fragment 가 보여질때 호출 되는 Callback
    abstract void onDisplay();

    //Fragment 이동 시 이동 전 Fragment 가 dismiss 된 것을 체크 하기 위한 Callback
    abstract void onDismiss();
}
