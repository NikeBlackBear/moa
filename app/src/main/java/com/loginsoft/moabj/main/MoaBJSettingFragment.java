package com.loginsoft.moabj.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Account;
import com.google.android.gms.plus.Plus;
import com.loginsoft.moabj.R;
import com.loginsoft.moabj.Util.LogUtil;
import com.loginsoft.moabj.Util.MoaBJPreferenceManager;
import com.loginsoft.moabj.common.CommonValues;
import com.loginsoft.moabj.custom.TypefaceTextView;

/**
 * Created by Administrator on 2015-02-02.
 */
public class MoaBJSettingFragment extends MoaBJDataObserverFragment implements CommonValues, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, Account {
    private final int REQUEST_CODE_RESOLVE_ERR = 9000;
    private ImageView mAlarmSwView;
    private ImageView mYoutubeLoginSwView;
    private TypefaceTextView mVersionView;
    private TypefaceTextView mYoutubeLoginTitleView;
    private RelativeLayout mKakaoShareBtn;
    private RelativeLayout mFacebookShareBtn;
    private GoogleApiClient mGoogleApiClient;
    private ConnectionResult mConnectionResult;
    private boolean mIntentInProgress;
    private boolean mSignInClicked;
    private ProgressDialog mConnectionProgressDialog;
    private ProgressDialog mDisconnectionProgressDialog;
    private Tracker mTracker;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        LogUtil.LOGE("MoaBJSettingFragment() :: onCreateView()");
        return inflater.inflate(R.layout.fragment_setting, null);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();

        mTracker = ((MoaBJAnalystics) getActivity().getApplication()).getTracker(getActivity(), MoaBJAnalystics.TrackerName.APP_TRACKER);
        mTracker.setScreenName("MoaBJSettingFragment");
        mTracker.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    void onBookMarkChanged(String bookMarkActionType, String bookMarkContentType, String id) {
        LogUtil.LOGE("MoaBJSettingFragment() :: onBookMarkChanged() :: bookMarkActionType = " + bookMarkActionType + " bookMarkContentType = " + bookMarkContentType + " id = " + id);
    }

    @Override
    void onDisplay() {
        LogUtil.LOGE("MoaBJSettingFragment() :: onDisplay()");
        GoogleAnalytics.getInstance(getActivity()).reportActivityStart(getActivity());
    }

    @Override
    void onDismiss() {
        LogUtil.LOGE("MoaBJSettingFragment() :: onDismiss()");
        GoogleAnalytics.getInstance(getActivity()).reportActivityStop(getActivity());
    }

    private void init() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        // 연결 실패가 해결되지 않은 경우 표시되는 진행률 표시줄입니다.
        mConnectionProgressDialog = new ProgressDialog(getActivity());
        mConnectionProgressDialog.setMessage(getResources().getString(R.string.str_login));

        mDisconnectionProgressDialog = new ProgressDialog(getActivity());
        mDisconnectionProgressDialog.setMessage(getResources().getString(R.string.str_logout));
        View root = getView();
        mAlarmSwView = (ImageView) root.findViewById(R.id.alarm_sw);
        mYoutubeLoginTitleView = (TypefaceTextView) root.findViewById(R.id.youtube_tv);
        mAlarmSwView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().sendBroadcast(new Intent(ACTION_ALARAM_STATE_CHANGED));
                if (MoaBJPreferenceManager.getAlarmState(getActivity()).equals(ALARM_SWITCH_ON)) {
                    MoaBJPreferenceManager.saveAlarmState(getActivity(), ALARM_SWITCH_OFF);
                    mAlarmSwView.setImageResource(R.drawable.switch_off);
                    LogUtil.LOGE("MoaBJSettingFragment :: AlarmState = " + MoaBJPreferenceManager.getAlarmState(getActivity()));
                } else if (MoaBJPreferenceManager.getAlarmState(getActivity()).equals(ALARM_SWITCH_OFF)) {
                    MoaBJPreferenceManager.saveAlarmState(getActivity(), ALARM_SWITCH_ON);
                    mAlarmSwView.setImageResource(R.drawable.switch_on);
                    LogUtil.LOGE("MoaBJSettingFragment :: AlarmState = " + MoaBJPreferenceManager.getAlarmState(getActivity()));
                }
            }
        });
        mVersionView = (TypefaceTextView) root.findViewById(R.id.version_tv);
        mVersionView.setText(getVersion());

        mYoutubeLoginSwView = (ImageView) root.findViewById(R.id.youtube_iv);
        mYoutubeLoginSwView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MoaBJPreferenceManager.getLoginState(getActivity()).equals(SIGN_OUT)) {
                    singInForGoogle();
                    mConnectionProgressDialog.show();
                    LogUtil.LOGE("MoaBJSettingFragment :: LoginState = " + MoaBJPreferenceManager.getLoginState(getActivity()));
                    mTracker.send(new HitBuilders.EventBuilder().setCategory("MoaBJSettingFragment").setAction("Youtube Login").setLabel("Sign in").build());
                } else if (MoaBJPreferenceManager.getLoginState(getActivity()).equals(SIGN_IN)) {
                    mDisconnectionProgressDialog.show();
                    singOutFromGoogle();
                    MoaBJPreferenceManager.saveLoginState(getActivity(), SIGN_OUT);
                    LogUtil.LOGE("MoaBJSettingFragment :: LoginState = " + MoaBJPreferenceManager.getLoginState(getActivity()));
                    mYoutubeLoginSwView.setImageResource(R.drawable.switch_off);
                    mYoutubeLoginTitleView.setText("YouTube");
                    mTracker.send(new HitBuilders.EventBuilder().setCategory("MoaBJSettingFragment").setAction("Youtube Login").setLabel("Sign out").build());
                }
            }
        });

        mKakaoShareBtn = (RelativeLayout) root.findViewById(R.id.kakao_share_view);
        mFacebookShareBtn = (RelativeLayout) root.findViewById(R.id.facebook_share_view);
        mKakaoShareBtn.setOnClickListener(mShareBtnListener);
        mFacebookShareBtn.setOnClickListener(mShareBtnListener);
    }

    private String getVersion() {
        String version = "";
        try {
            PackageInfo i = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            version = i.versionName;
        } catch (PackageManager.NameNotFoundException e) {
        }
        return version;
    }

    private void singInForGoogle() {
        if (!mGoogleApiClient.isConnected()) {
            if (mConnectionResult == null) {
                mSignInClicked = true;
                resolveSingInError();
            } else {
                try {
                    mConnectionResult.startResolutionForResult(this.getActivity(), REQUEST_CODE_RESOLVE_ERR);
                } catch (IntentSender.SendIntentException e) {
                    // 다시 연결을 시도합니다.
                    mConnectionResult = null;
                    mGoogleApiClient.connect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void singOutFromGoogle() {
        if (mGoogleApiClient.isConnected()) {
            // 연결을 해제하기 전에 clearDefaultAccount()를 실행합니다.
            mGoogleApiClient.clearDefaultAccountAndReconnect();
            mGoogleApiClient.disconnect();
            //mGoogleApiClient.connect();
        }
        mDisconnectionProgressDialog.dismiss();
        Toast.makeText(this.getActivity(), " 로그아웃 하셨습니다.", Toast.LENGTH_SHORT).show();
        MoaBJPreferenceManager.saveAccountName(getActivity(), SIGN_OUT_STATE);
    }

    @Override
    public void onStart() {
        super.onStart();
        LogUtil.LOGE("MoaBJSettingFragment :: onStart()");
        mGoogleApiClient.connect();
    }

    @Override
    public void onResume() {
        super.onResume();
        LogUtil.LOGE("MoaBJSettingFragment :: onResume()");
        mGoogleApiClient.connect();
        if (mConnectionProgressDialog.isShowing()) {
            mConnectionProgressDialog.dismiss();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private void resolveSingInError() {
        if (mConnectionResult != null) {
            if (mConnectionResult.hasResolution()) {
                try {
                    mIntentInProgress = true;
                    mConnectionResult.startResolutionForResult(this.getActivity(), REQUEST_CODE_RESOLVE_ERR);
                } catch (IntentSender.SendIntentException e) {
                    mIntentInProgress = false;
                    mGoogleApiClient.connect();
                }
            }
        } else {
            mIntentInProgress = false;
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (!result.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), getActivity(), 0).show();
            return;
        }
        if (!mIntentInProgress) {
            mConnectionResult = result;
            if (mSignInClicked) {
                resolveSingInError();
            }
        }
        // 사용자가 로그인 버튼을 클릭할 때 활동을 시작할 수 있도록
        // 인텐트를 저장합니다.
        mConnectionResult = result;
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        MoaBJPreferenceManager.saveLoginState(getActivity(), SIGN_IN);
        LogUtil.LOGE("MoaBJSettingFragment :: LoginState = " + MoaBJPreferenceManager.getLoginState(getActivity()));
        mYoutubeLoginSwView.setImageResource(R.drawable.switch_on);
        mYoutubeLoginTitleView.setText("YouTube 로그인");
        mSignInClicked = false;
        mConnectionProgressDialog.dismiss();
        MoaBJPreferenceManager.saveAccountName(getActivity(), Plus.AccountApi.getAccountName(mGoogleApiClient));
    }

    @Override
    public void onConnectionSuspended(int i) {
        mDisconnectionProgressDialog.dismiss();
        Toast.makeText(this.getActivity(), "구글 계정과 연결되지 않았습니다.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onActivityResult(int requestCode, int responseCode, Intent intent) {
        if (requestCode == REQUEST_CODE_RESOLVE_ERR && responseCode == super.getActivity().RESULT_OK) {
            mConnectionResult = null;
            mGoogleApiClient.connect();
        }
    }

    @Override
    public PendingResult<Status> revokeAccessAndDisconnect(GoogleApiClient googleApiClient) {
        return null;
    }

    @Override
    public void clearDefaultAccount(GoogleApiClient googleApiClient) {

    }

    @Override
    public String getAccountName(GoogleApiClient googleApiClient) {
        return null;
    }

    private View.OnClickListener mShareBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.equals(mFacebookShareBtn)) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/sharer/sharer.php?u=https://play.google.com/store/apps/details?id=com.loginsoft.moabj"));
                startActivity(intent);
            } else if (v.equals(mKakaoShareBtn)) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT, "모아BJ");
                intent.putExtra(Intent.EXTRA_TEXT, "아프리카 인기 BJ 들이 한자리에!!! https://play.google.com/store/apps/details?id=com.loginsoft.moabj");
                intent.setPackage("com.kakao.talk");
                startActivity(intent);
            }

//            Intent shareIntent = new Intent(Intent.ACTION_SEND);
//            shareIntent.addCategory(Intent.CATEGORY_DEFAULT);
//            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "주제");
//            shareIntent.putExtra(Intent.EXTRA_TEXT, "내용");
//            shareIntent.putExtra(Intent.EXTRA_TITLE, "제목");
//            shareIntent.setType("text/plain");
//            shareIntent.putExtra("extraText","ee");
//            startActivity(Intent.createChooser(shareIntent, "공유하기"));
        }
    };
}
