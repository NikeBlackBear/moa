package com.loginsoft.moabj.common;

/**
 * Created by Administrator on 2015-02-03.
 */
public interface CommonValues {
    //REQUEST PARAM
    public static final String PARAM_COUNT = "cnt";
    public static final String PARAM_PAGE_START_NUM = "startNum";
    public static final String PARAM_BJ_COUNT = "pageSize";
    public static final String PARAM_CHANNEL_ID = "channelID";
    public static final String PARAM_PLAY_LIST_ID = "playlistID";
    public static final String PARAM_PAGE_TOKEN = "pageToken";
    public static final String PARAM_VIDEO_ID = "videoID";
    public static final String PARAM_SEARCH_STR = "searchStr";
    public static final String PARAM_BJ_ICON = "bjicon";
    public static final String PARAM_BJ_PARTNER_TYPE = "isPartnerBJ";

    //RESPONSE PARAM
    public static final String PARAM_VERSION = "get_v_version";

    //INTENT Extra
    public static final String EXTRA_HOT_VIDEO = "extra_hot_video";
    public static final String EXTRA_SUB_POSITION = "extra_sub_position";
    public static final String EXTRA_BJ_ITEM = "extra_bj_item";
    public static final String EXTRA_BOOKMARK_CONTENT_TYPE = "extra_bookmark_content_type";
    public static final String EXTRA_BOOKMARK_ACTION_TYPE = "extra_bookmark_action_type";
    public static final String EXTRA_BOOKMARK_ID = "extra_bookmark_id";

    //SharedPreference
    public static final String PREF_LOGIN = "login_preference";
    public static final String PREF_ALRAM = "alarm_preference";
    public static final String PREF_ACCOUNT_NAME = "account_name_preference";
    public static final String LOGIN_KEY = "LOGIN_KEY";
    public static final String SIGN_IN = "IN";
    public static final String SIGN_OUT = "OUT";
    public static final String ALARM_SWITCH_ON = "ON";
    public static final String ALARM_SWITCH_OFF = "OFF";
    public static final String ALARM_STATE = "alarm_state";
    public static final String ACCOUNT_NAME = "account_name";
    public static final String SIGN_OUT_STATE = "SIGN_OUT";

    public static final String ACTION_ALARAM_STATE_CHANGED = "com.loginsoft.moabj.action.ALARAM_STATE_CHANGED";
    public static final String ACTION_ALARM_NOTIFICATION = "com.loginsoft.moabj.action.ALARM_NOTIFICATION";
    public static final String ACTION_BOOKMARK_CHANGED = "com.loginsoft.moabj.action.BOOKMARK_CHANGED";

    public static final String FONT_THIN_NANUM_GOTHIC = "ThinNanumBarunGothic.ttf";
    public static final String FONT_NANUM_GOTHIC = "NanumBarunGothic.ttf";

    public static final String VALUE_EMPTY = "empty";
    public static final String VALUE_NO_NOTICE = "nonotice";

    public static final String BOOKMARK_ACTION_TYPE_ADD = "add";
    public static final String BOOKMARK_ACTION_TYPE_DELETE = "delete";
    public static final String BOOKMARK_CONTENT_TYPE_BJ = "bj";
    public static final String BOOKMARK_CONTENT_TYPE_VIDEO = "video";

    public static final int FRAGMENT_BJ_LIST = 2;

    public static final int TYPE_MAIN_LIST = 100;
    public static final int TYPE_SEARCH_LIST = 101;

    public static final String TYPE_PARTNER = "Y";
    public static final String TYPE_NON_PARTNER = "N";
    public static final String HAS_PLAY_LIST = "hasPlayList";
    public static final String NO_PLAY_LIST = "noPlayList";
    public static final String NO_HAS_SUBSCRIPTION = "NoHasSubscription";

}
