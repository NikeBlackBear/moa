package com.loginsoft.moabj.custom;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.Scopes;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.YouTubeScopes;
import com.google.api.services.youtube.model.ResourceId;
import com.google.api.services.youtube.model.Subscription;
import com.google.api.services.youtube.model.SubscriptionListResponse;
import com.google.api.services.youtube.model.SubscriptionSnippet;
import com.loginsoft.moabj.R;
import com.loginsoft.moabj.Util.LogUtil;
import com.loginsoft.moabj.Util.MoaBJPreferenceManager;
import com.loginsoft.moabj.common.CommonValues;
import com.loginsoft.moabj.database.DataManager;
import com.loginsoft.moabj.http.ParseManager;
import com.loginsoft.moabj.main.MoaBJAnalystics;
import com.loginsoft.moabj.main.MoaBJDetailBJActivity;
import com.loginsoft.moabj.model.BJInfo;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created by Administrator on 2015-03-16.
 */
public class PartnerBJAlertDialog extends Dialog implements CommonValues {
    private final int REQUEST_AUTHORIZATION = 3;
    private final String COMMAND_CHECK_SUBSCRIPTION = "check";
    private final String COMMAND_SUBSCRIPTION = "sub";
    private final String COMMAND_DISSUBSCRIPTION = "dis";

    private ImageViewer mBackgroundBJImage;
    private ImageView mYoutubeSubscriptionBtn;
    private ImageView mGoAfreecaBtn;
    private ImageView mFavoriteBtn;
    private CircleNetworkImageView mMainBJImage;
    private TypefaceTextView mBJNameView;
    private TypefaceTextView mBJDescription;
    private TypefaceTextView mCloseButton;
    private TypefaceTextView mSelectPartnerBjButton;
    private BJInfo mBJInfo;

    private DataManager mDataManager;

    private BJInfo mTargetBJInfo = new BJInfo();

    private String mSubscriptionOption = null;
    private int mCheckSubscription = 0;

    private GoogleAccountCredential mGoogleCredential;
    private final JsonFactory mJsonFactory = new GsonFactory();
    private final HttpTransport mTransport = AndroidHttp.newCompatibleTransport();

    private Activity mActivity;
    private Tracker mTracker;

    public PartnerBJAlertDialog(Activity activity, BJInfo BJInfo) {
        super(activity);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        mActivity = activity;
        mBJInfo = BJInfo;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_partner_bj);
        init();

        mTracker = ((MoaBJAnalystics) mActivity.getApplication()).getTracker(mActivity, MoaBJAnalystics.TrackerName.APP_TRACKER);
        mTracker.setScreenName("PartnerBJAlertDialog()");
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(mActivity).reportActivityStart(mActivity);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(mActivity).reportActivityStop(mActivity);
    }

    private void init() {
        mDataManager = new DataManager(getContext());
        mBackgroundBJImage = (ImageViewer) findViewById(R.id.frame_bj_image);
        mYoutubeSubscriptionBtn = (ImageView) findViewById(R.id.youtube_subscription_btn);
        mGoAfreecaBtn = (ImageView) findViewById(R.id.go_afreeca_btn);
        mFavoriteBtn = (ImageView) findViewById(R.id.favorite_btn);
        mMainBJImage = (CircleNetworkImageView) findViewById(R.id.bj_profile_image);
        mBJNameView = (TypefaceTextView) findViewById(R.id.bj_name);
        mBJDescription = (TypefaceTextView) findViewById(R.id.bj_description);
        mCloseButton = (TypefaceTextView) findViewById(R.id.close_btn);
        mSelectPartnerBjButton = (TypefaceTextView) findViewById(R.id.select_partner_bj_btn);

        mYoutubeSubscriptionBtn.setOnClickListener(mSubBtnListener);
        mGoAfreecaBtn.setOnClickListener(mSubBtnListener);
        mFavoriteBtn.setOnClickListener(mSubBtnListener);
        mCloseButton.setOnClickListener(mPartnerBJBtnListener);
        mSelectPartnerBjButton.setOnClickListener(mPartnerBJBtnListener);

        mBJDescription.setSelected(true);

        setBJInfo(mBJInfo);
        mSubscriptionOption = COMMAND_CHECK_SUBSCRIPTION;
        if (!MoaBJPreferenceManager.getAccountName(getContext()).equals(SIGN_OUT_STATE)) {
            new SubscriptTask().execute();
        }
    }

    private void setBJInfo(BJInfo BJInfo) {
        if (BJInfo != null) {
            mTargetBJInfo = BJInfo;
            mBackgroundBJImage.setImageURL(mTargetBJInfo.getThumnailurl());
            mMainBJImage.loadCircleBitmap(mTargetBJInfo.getThumnailurl());
            mBJNameView.setText(mTargetBJInfo.getBjName());
            mBJDescription.setText(mTargetBJInfo.getbjsogae().equals("") ? getContext().getText(R.string.str_empty_bj_desc) : mTargetBJInfo.getbjsogae());
            if (mTargetBJInfo.isFavorite()) {
                mFavoriteBtn.setImageResource(R.drawable.bookmark_btn_on);
            } else {
                mFavoriteBtn.setImageResource(R.drawable.bookmark_btn_off);
            }

            if (mTargetBJInfo.getAfracaURL().equals("")) {
                mGoAfreecaBtn.setVisibility(View.GONE);
            }
        } else {
            LogUtil.LOGD("MoaBJDetailBJActivity :: loadBJInfo() :: intent null");
        }
    }

    public DataManager getDataManager() {
        return mDataManager;
    }


    private void sendBookMarkBroadcast(String bookMarkActionType, String bookMarkContent, String id) {
        Intent intent = new Intent(ACTION_BOOKMARK_CHANGED);
        intent.putExtra(EXTRA_BOOKMARK_CONTENT_TYPE, bookMarkContent);
        intent.putExtra(EXTRA_BOOKMARK_ACTION_TYPE, bookMarkActionType);
        intent.putExtra(EXTRA_BOOKMARK_ID, id);
        getContext().sendBroadcast(intent);
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mDataManager != null) {
            mDataManager.close();
        }
    }

    private final View.OnClickListener mPartnerBJBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.close_btn:
                    dismiss();
                    break;
                case R.id.select_partner_bj_btn:
                    Intent intent = new Intent(getContext(), MoaBJDetailBJActivity.class);
                    intent.putExtra(EXTRA_BJ_ITEM, mBJInfo);
                    getContext().startActivity(intent);
                    dismiss();
                    break;
            }
        }
    };

    private final View.OnClickListener mSubBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.youtube_subscription_btn:
                    if (MoaBJPreferenceManager.getAccountName(getContext()).equals(SIGN_OUT_STATE)) {
                        Toast.makeText(getContext(), R.string.str_no_signin, Toast.LENGTH_SHORT).show();
                    } else {
                        LogUtil.LOGD("onClickListener :: mCheckSubscription = " + Integer.toString(mCheckSubscription));
                        LogUtil.LOGD("onClickListener :: getAccountName() :: " + MoaBJPreferenceManager.getAccountName(getContext()));
                        mYoutubeSubscriptionBtn.setEnabled(false);
                        if (mCheckSubscription == 0) {
                            mSubscriptionOption = COMMAND_SUBSCRIPTION;
                            new SubscriptTask().execute();
                            mTracker.send(new HitBuilders.EventBuilder().setCategory("PartnerBJAlertDialog").setAction("Subscription").setLabel("Do Subscription").build());
                        } else if (mCheckSubscription == 1) {
                            mSubscriptionOption = COMMAND_DISSUBSCRIPTION;
                            new SubscriptTask().execute();
                            mTracker.send(new HitBuilders.EventBuilder().setCategory("PartnerBJAlertDialog").setAction("Subscription").setLabel("Do DisSubscription").build());
                        }
                    }
                    break;
                case R.id.go_afreeca_btn:
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mTargetBJInfo.getAfracaURL()));
                    getContext().startActivity(intent);
                    break;

                case R.id.favorite_btn:
                    if (mTargetBJInfo.isFavorite()) {
                        Toast.makeText(getContext(), R.string.str_favorite_delete, Toast.LENGTH_SHORT).show();
                        mDataManager.deleteFavoriteBjInfo(mTargetBJInfo.getBJChannelID());
                        mFavoriteBtn.setImageResource(R.drawable.bookmark_btn_off);
                        mTargetBJInfo.setFavorite(false);
                        sendBookMarkBroadcast(BOOKMARK_ACTION_TYPE_DELETE, BOOKMARK_CONTENT_TYPE_BJ, mTargetBJInfo.getBJChannelID());
                    } else {
                        Toast.makeText(getContext(), R.string.str_favorite_add, Toast.LENGTH_SHORT).show();
                        mDataManager.insertFavoriteBJ(mTargetBJInfo);
                        mFavoriteBtn.setImageResource(R.drawable.bookmark_btn_on);
                        mTargetBJInfo.setFavorite(true);
                        sendBookMarkBroadcast(BOOKMARK_ACTION_TYPE_ADD, BOOKMARK_CONTENT_TYPE_BJ, mTargetBJInfo.getBJChannelID());
                    }
                    break;
            }
        }
    };

    class SubscriptTask extends AsyncTask<Void, Void, Boolean> {

        private final String[] SCOPES = {Scopes.PROFILE, YouTubeScopes.YOUTUBE};

        public SubscriptTask() {
            mGoogleCredential = GoogleAccountCredential.usingOAuth2(getContext(), Arrays.asList(SCOPES));
            mGoogleCredential.setSelectedAccountName(MoaBJPreferenceManager.getAccountName(getContext()));
            mGoogleCredential.setBackOff(new ExponentialBackOff());
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            final YouTube youtube =
                    new YouTube.Builder(mTransport, mJsonFactory, mGoogleCredential).setApplicationName(getContext().getString(R.string.app_name)).build();

            ResourceId resourceId = new ResourceId();
            resourceId.setChannelId(mTargetBJInfo.getBJChannelID());
            resourceId.setKind("youtube#channel");

            SubscriptionSnippet snippet = new SubscriptionSnippet();
            snippet.setResourceId(resourceId);

            try {
                Subscription subscription = new Subscription();
                subscription.setSnippet(snippet);
                if (mSubscriptionOption.equals(COMMAND_CHECK_SUBSCRIPTION)) {
                    YouTube.Subscriptions.List subscriptionList = youtube.subscriptions().list("id,snippet,contentDetails").setForChannelId(mTargetBJInfo.getBJChannelID()).setMine(true);
                    SubscriptionListResponse responseOfSubscriptionIdForJson = subscriptionList.execute();
                    LogUtil.LOGD("mCheckSubscription :: " + responseOfSubscriptionIdForJson.toString());
                    mCheckSubscription = new ParseManager().parseSubscriptionValue(responseOfSubscriptionIdForJson.toString());
                    LogUtil.LOGD("CheckSubscriptTask() :: CheckSubscription :: " + Integer.toString(mCheckSubscription));
                    return true;
                } else if (mSubscriptionOption.equals(COMMAND_SUBSCRIPTION)) {
                    LogUtil.LOGD("before :: subscription =  " + subscription + " subscription.toString() = " + subscription.toString());
                    YouTube.Subscriptions.Insert subscriptionInsert = youtube.subscriptions().insert("snippet,contentDetails", subscription);
                    Subscription returnSubscription = subscriptionInsert.execute();
                    LogUtil.LOGD("after :: returnSubScription =  " + returnSubscription + " returnSubscription.toString() = " + returnSubscription.toString());
                    return true;
                } else if (mSubscriptionOption.equals(COMMAND_DISSUBSCRIPTION)) {
                    YouTube.Subscriptions.List subscriptionList = youtube.subscriptions().list("id,snippet,contentDetails").setForChannelId(mTargetBJInfo.getBJChannelID()).setMine(true);
                    SubscriptionListResponse responseOfSubscriptionIdForJson = subscriptionList.execute();
                    if (responseOfSubscriptionIdForJson.toString() == NO_HAS_SUBSCRIPTION) {
                        return false;
                    }
                    YouTube.Subscriptions.Delete subscriptionDelete = youtube.subscriptions().delete(new ParseManager().parseDeleteSubscriptionID(responseOfSubscriptionIdForJson.toString()));
                    subscriptionDelete.execute();
                    return true;
                }
            } catch (GoogleJsonResponseException e) {
                e.printStackTrace();
            } catch (UserRecoverableAuthIOException e) {
                e.printStackTrace();
                mActivity.startActivityForResult(e.getIntent(), REQUEST_AUTHORIZATION);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }


        @Override
        protected void onPostExecute(Boolean isProcessSuccess) {
            super.onPostExecute(isProcessSuccess);
            if (mSubscriptionOption.equals(COMMAND_CHECK_SUBSCRIPTION)) {
                if (isProcessSuccess) {
                    if (mCheckSubscription == 0) {
                        mYoutubeSubscriptionBtn.setImageResource(R.drawable.icon_youtube);
                        if (mTargetBJInfo.getPartnertype().equals(TYPE_PARTNER)) {
                        }
                    } else if (mCheckSubscription == 1) {
                        mYoutubeSubscriptionBtn.setImageResource(R.drawable.icon_youtube_sub);
                    }
                } else {
                    mYoutubeSubscriptionBtn.setImageResource(R.drawable.icon_youtube);
                }
            } else if (mSubscriptionOption.equals(COMMAND_SUBSCRIPTION)) {
                if (isProcessSuccess) {
                    mYoutubeSubscriptionBtn.setImageResource(R.drawable.icon_youtube_sub);
                    mCheckSubscription = 1;
                    Toast.makeText(getContext(), R.string.str_complete_subscription, Toast.LENGTH_SHORT).show();
                } else {
                    mYoutubeSubscriptionBtn.setImageResource(R.drawable.icon_youtube);
                    mCheckSubscription = 0;
                }
            } else if (mSubscriptionOption.equals(COMMAND_DISSUBSCRIPTION)) {
                if (!isProcessSuccess) {
                    mYoutubeSubscriptionBtn.setImageResource(R.drawable.icon_youtube_sub);
                    mCheckSubscription = 1;
                    Toast.makeText(getContext(), R.string.str_fail_dissubscription, Toast.LENGTH_SHORT).show();
                } else {
                    mYoutubeSubscriptionBtn.setImageResource(R.drawable.icon_youtube);
                    mCheckSubscription = 0;
                    Toast.makeText(getContext(), R.string.str_complete_dissubscription, Toast.LENGTH_SHORT).show();
                }
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mYoutubeSubscriptionBtn.setEnabled(true);
                }
            }, 10000);
        }
    }
}
