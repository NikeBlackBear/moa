package com.loginsoft.moabj.custom;

/**
 * Created by Administrator on 2015-02-17.
 */
public interface OnDeleteBookMarkListener {
    public void onDelete(int position, Object deleteItem);
}
