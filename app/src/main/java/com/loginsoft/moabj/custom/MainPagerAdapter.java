package com.loginsoft.moabj.custom;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.loginsoft.moabj.Util.LogUtil;
import com.loginsoft.moabj.common.CommonValues;
import com.loginsoft.moabj.main.MoaBJBookMarkFragment;
import com.loginsoft.moabj.main.MoaBJListFragment;
import com.loginsoft.moabj.main.MoaBJMainFragment;
import com.loginsoft.moabj.main.MoaBJSettingFragment;
import com.loginsoft.moabj.main.MoaBJTopTwentyFragment;

import java.util.ArrayList;

/**
 * Created by Administrator on 2015-02-02.
 */
public class MainPagerAdapter extends FragmentStatePagerAdapter implements CommonValues {
    private ArrayList<Fragment> mFragmentList;

    public MainPagerAdapter(FragmentManager fm) {
        super(fm);
        mFragmentList = new ArrayList<Fragment>();
        mFragmentList.add(new MoaBJMainFragment());
        mFragmentList.add(new MoaBJTopTwentyFragment());
        mFragmentList.add(new MoaBJListFragment());
        mFragmentList.add(new MoaBJBookMarkFragment());
        mFragmentList.add(new MoaBJSettingFragment());
    }

    @Override
    public Fragment getItem(int position) {
        LogUtil.LOGD("MainPagerAdapter :: getItem() :: position = " + position);
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }
}
