package com.loginsoft.moabj.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.loginsoft.moabj.R;
import com.loginsoft.moabj.Util.FontFactory;
import com.loginsoft.moabj.common.CommonValues;

/**
 * Created by Administrator on 2015-02-10.
 */
public class TypefaceTextView extends TextView implements CommonValues {
    public TypefaceTextView(Context context) {
        this(context, null, 0);
    }

    public TypefaceTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TypefaceTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeface(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
    }

    private void setTypeface(Context context, AttributeSet attrs) {
        TypedArray arr = context.obtainStyledAttributes(attrs, R.styleable.TypefaceTextView);
        String typefaceName = arr.getString(R.styleable.TypefaceTextView_typeface);
        Typeface typeface;
        if (typefaceName != null && !typefaceName.equals("")) {
            typeface = FontFactory.getInstance(context).getTypeface(typefaceName);
        } else {
            typeface = FontFactory.getInstance(context).getTypeface(FONT_THIN_NANUM_GOTHIC);
        }
        setTypeface(typeface);
    }
}
