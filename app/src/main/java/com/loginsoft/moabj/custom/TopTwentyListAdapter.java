package com.loginsoft.moabj.custom;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.loginsoft.moabj.R;
import com.loginsoft.moabj.Util.DateTime;
import com.loginsoft.moabj.common.CommonValues;
import com.loginsoft.moabj.database.DataManager;
import com.loginsoft.moabj.main.MoaBJDetailBJActivity;
import com.loginsoft.moabj.main.MoaBJMainFrame;
import com.loginsoft.moabj.model.HotVideo;

import java.text.NumberFormat;
import java.util.List;

/**
 * Created by Administrator on 2015-02-03.
 */
public class TopTwentyListAdapter extends BaseAdapter implements CommonValues {
    private Activity mActivity;
    private List<HotVideo> mTop20List;
    private LayoutInflater mInflater;
    private DataManager mDataManager;

    public TopTwentyListAdapter(Activity activity, List<HotVideo> list) {
        mActivity = activity;
        mInflater = LayoutInflater.from(activity);
        mTop20List = list;
        if (activity instanceof MoaBJMainFrame) {
            mDataManager = ((MoaBJMainFrame) activity).getDataManager();
        } else if (activity instanceof MoaBJDetailBJActivity) {
            mDataManager = ((MoaBJDetailBJActivity) activity).getDataManager();
        }
    }

    @Override
    public int getCount() {
        return mTop20List.size();
    }

    @Override
    public HotVideo getItem(int i) {
        return mTop20List.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = mInflater.inflate(R.layout.toptwentycontent, null);
            holder = new ViewHolder();
            holder.Channel_title = (TypefaceTextView) view.findViewById(R.id.contentbj);
            holder.date = (TypefaceTextView) view.findViewById(R.id.contentdate);
            holder.rank = (TypefaceTextView) view.findViewById(R.id.contentlank);
            holder.title = (TypefaceTextView) view.findViewById(R.id.contenttitle);
            holder.hits = (TypefaceTextView) view.findViewById(R.id.contenthits);
            holder.favorite = (ImageView) view.findViewById(R.id.contentfavorite);
            holder.videoThumbnail = (ImageViewer) view.findViewById(R.id.contentimage);
            holder.duration = (TypefaceTextView) view.findViewById(R.id.contenttime);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.position = i;
        view.setTag(holder);

        holder.Channel_title.setText(getItem(i).getChannelTitle());
        holder.videoThumbnail.setImageURL(getItem(i).getVideoThumbnail());
        holder.date.setText(DateTime.getUploadDateCompareCurrentDate(mActivity, getItem(i).getUploadDate()));
        holder.title.setText(getItem(i).getTitle());
        try {
            holder.hits.setText(NumberFormat.getInstance().format(Integer.valueOf(getItem(i).getViewCount())));
        } catch (Exception e) {
            holder.hits.setText("?");
        }
        holder.duration.setText(getItem(i).getDuration());
        holder.rank.setText(String.valueOf(i + 1));
        if (getItem(i).isFavorite()) {
            holder.favorite.setImageResource(R.drawable.bookmark_btn_on);
        } else {
            holder.favorite.setImageResource(R.drawable.bookmark_btn_off);
        }
        holder.favorite.setOnClickListener(mBookMarkClickListener);
        return view;
    }

    private void sendBookMarkBroadcast(String bookMarkActionType, String bookMarkContent, String id) {
        Intent intent = new Intent(ACTION_BOOKMARK_CHANGED);
        intent.putExtra(EXTRA_BOOKMARK_CONTENT_TYPE, bookMarkContent);
        intent.putExtra(EXTRA_BOOKMARK_ACTION_TYPE, bookMarkActionType);
        intent.putExtra(EXTRA_BOOKMARK_ID, id);
        mActivity.sendBroadcast(intent);
    }

    private final View.OnClickListener mBookMarkClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            View parent = (View) v.getParent().getParent();
            ViewHolder holder = (ViewHolder) parent.getTag();
            HotVideo item = mTop20List.get(holder.position);
            if (getItem(holder.position).isFavorite()) {
                Toast.makeText(mActivity, R.string.str_favorite_delete, Toast.LENGTH_SHORT).show();
                mDataManager.deleteFavoriteVideoInfo(getItem(holder.position).getVideoID());
                holder.favorite.setImageResource(R.drawable.bookmark_btn_off);
                item.setFavorite(false);
                sendBookMarkBroadcast(BOOKMARK_ACTION_TYPE_DELETE, BOOKMARK_CONTENT_TYPE_VIDEO, getItem(holder.position).getVideoID());
            } else {
                mDataManager.insertFavoriteVideoData(getItem(holder.position));
                Toast.makeText(mActivity, R.string.str_favorite_add, Toast.LENGTH_SHORT).show();
                holder.favorite.setImageResource(R.drawable.bookmark_btn_on);
                item.setFavorite(true);
                sendBookMarkBroadcast(BOOKMARK_ACTION_TYPE_ADD, BOOKMARK_CONTENT_TYPE_VIDEO, getItem(holder.position).getVideoID());
            }
            mTop20List.set(holder.position, item);
        }
    };


    class ViewHolder {
        public TypefaceTextView rank;
        public TypefaceTextView duration;
        public TypefaceTextView Channel_title;
        public TypefaceTextView title;
        public TypefaceTextView hits;
        public ImageViewer videoThumbnail;
        public TypefaceTextView date;
        public ImageView favorite;
        public int position;
    }
}
