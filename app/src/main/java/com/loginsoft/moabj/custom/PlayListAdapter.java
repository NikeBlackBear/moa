package com.loginsoft.moabj.custom;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.loginsoft.moabj.R;
import com.loginsoft.moabj.Util.DateTime;
import com.loginsoft.moabj.model.BJInfo;
import com.loginsoft.moabj.model.PlayListInfo;

import java.util.List;

/**
 * Created by Administrator on 2015-02-08.
 */
public class PlayListAdapter extends BaseAdapter {
    private Activity mActivity;
    private List<PlayListInfo> mBjList;
    private BJInfo mBJInfo;
    private LayoutInflater mInflater;

    public PlayListAdapter(Activity activity, List<PlayListInfo> list, BJInfo bjInfo) {
        mActivity = activity;
        mInflater = LayoutInflater.from(activity);
        mBjList = list;
        mBJInfo = bjInfo;
    }

    @Override
    public int getCount() {
        return mBjList.size();
    }

    @Override
    public PlayListInfo getItem(int position) {
        return mBjList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = mInflater.inflate(R.layout.play_list_row, null);
            holder = new ViewHolder();
            holder.playListThumbnail = (ImageViewer) view.findViewById(R.id.playlist_thumbnail);
            holder.videoCount = (TypefaceTextView) view.findViewById(R.id.video_count);
            holder.playListTitle = (TypefaceTextView) view.findViewById(R.id.playlist_title);
            holder.bjName = (TypefaceTextView) view.findViewById(R.id.bj_name);
            holder.uploadDate = (TypefaceTextView) view.findViewById(R.id.upload_date);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.playListThumbnail.setImageURL(getItem(i).getDefaultimage());
        holder.videoCount.setText(getItem(i).getItemCount() + "+");
        holder.playListTitle.setText(getItem(i).getTitle());
        holder.bjName.setText(mBJInfo.getBjName());
        holder.uploadDate.setText(DateTime.getUploadDateCompareCurrentDate(mActivity, getItem(i).getPublishedAt()));
        return view;
    }

    class ViewHolder {
        public TypefaceTextView playListTitle;
        public ImageViewer playListThumbnail;
        public TypefaceTextView videoCount;
        public TypefaceTextView bjName;
        public TypefaceTextView uploadDate;
    }
}
