package com.loginsoft.moabj.custom;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.loginsoft.moabj.R;
import com.loginsoft.moabj.Util.DateTime;
import com.loginsoft.moabj.Util.LogUtil;
import com.loginsoft.moabj.database.DataManager;
import com.loginsoft.moabj.main.MoaBJDetailBJActivity;
import com.loginsoft.moabj.main.MoaBJMainFrame;
import com.loginsoft.moabj.model.HotVideo;

import java.text.NumberFormat;
import java.util.List;

/**
 * Created by Administrator on 2015-02-09.
 */
public class FavoriteVideoAdapter extends BaseAdapter {
    private Activity mActivity;
    private List<HotVideo> mVideoList;
    private LayoutInflater mInflater;
    private DataManager mDataManager;
    private OnDeleteBookMarkListener mDeleteBookMarkListener;

    public FavoriteVideoAdapter(Activity activity, List<HotVideo> list) {
        mActivity = activity;
        mInflater = LayoutInflater.from(activity);
        mVideoList = list;
        if (activity instanceof MoaBJMainFrame) {
            mDataManager = ((MoaBJMainFrame) activity).getDataManager();
        } else if (activity instanceof MoaBJDetailBJActivity) {
            mDataManager = ((MoaBJDetailBJActivity) activity).getDataManager();
        }
    }

    public void setOnDeleteBookMarkListener(OnDeleteBookMarkListener listener) {
        mDeleteBookMarkListener = listener;
    }

    @Override
    public int getCount() {
        return mVideoList.size();
    }

    @Override
    public HotVideo getItem(int position) {
        return mVideoList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = mInflater.inflate(R.layout.favorite_video_row, null);
            holder = new ViewHolder();
            holder.videoImage = (ImageViewer) view.findViewById(R.id.contentimage);
            holder.videoTitle = (TextView) view.findViewById(R.id.contenttitle);
            holder.channelTitle = (TextView) view.findViewById(R.id.contentbj);
            holder.hits = (TextView) view.findViewById(R.id.contenthits);
            holder.updateDate = (TextView) view.findViewById(R.id.contentdate);
            holder.playTime = (TextView) view.findViewById(R.id.contenttime);
            holder.favoriteIcon = (ImageView) view.findViewById(R.id.contentfavorite);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.position = i;
        view.setTag(holder);

        holder.videoImage.setImageURL(getItem(i).getVideoThumbnail());
        holder.videoTitle.setText(getItem(i).getTitle());
        holder.channelTitle.setText(getItem(i).getChannelTitle());
        try {
            holder.hits.setText(NumberFormat.getInstance().format(Integer.parseInt(getItem(i).getViewCount())));
        } catch (Exception e) {
            e.printStackTrace();
            holder.hits.setText("?");
        }
        holder.updateDate.setText(DateTime.getUploadDateCompareCurrentDate(mActivity, getItem(i).getUploadDate()));
        holder.playTime.setText(getItem(i).getDuration());
        holder.favoriteIcon.setImageResource(R.drawable.bookmark_btn_on);
        holder.favoriteIcon.setOnClickListener(checkoutFavorite);
        return view;
    }

    private final View.OnClickListener checkoutFavorite = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            View parent = (View) v.getParent().getParent();
            ViewHolder holder = (ViewHolder) parent.getTag();
            LogUtil.LOGD("FavoriteVideoAdapter :: delete holder.position = " + holder.position + " getCount = " + getCount());
            mDataManager.deleteFavoriteVideoInfo(getItem(holder.position).getVideoID());
            if (mDeleteBookMarkListener != null) {
                mDeleteBookMarkListener.onDelete(holder.position, getItem(holder.position));
            }
        }
//        }
    };

    public class ViewHolder {
        public ImageViewer videoImage;
        public TextView videoTitle;
        public TextView channelTitle;
        public TextView hits;
        public TextView updateDate;
        public TextView playTime;
        public ImageView favoriteIcon;
        public int position;
    }
}
