package com.loginsoft.moabj.custom;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loginsoft.moabj.R;
import com.loginsoft.moabj.Util.LogUtil;
import com.loginsoft.moabj.common.CommonValues;
import com.loginsoft.moabj.database.DataManager;
import com.loginsoft.moabj.main.MoaBJDetailBJActivity;
import com.loginsoft.moabj.main.MoaBJMainFrame;
import com.loginsoft.moabj.model.BJInfo;

import java.text.NumberFormat;
import java.util.List;

/**
 * Created by Administrator on 2015-02-05.
 */
public class BJListAdapter extends BaseAdapter implements CommonValues {
    private Activity mActivity;
    private List<BJInfo> mBJList;
    private LayoutInflater mInflater;
    private DataManager mDataManager;

    public BJListAdapter(Activity activity, List<BJInfo> list) {
        mActivity = activity;
        mInflater = LayoutInflater.from(activity);
        mBJList = list;
        if (activity instanceof MoaBJMainFrame) {
            mDataManager = ((MoaBJMainFrame) activity).getDataManager();
        } else if (activity instanceof MoaBJDetailBJActivity) {
            mDataManager = ((MoaBJDetailBJActivity) activity).getDataManager();
        }
    }

    @Override
    public int getCount() {
        return mBJList.size();
    }

    @Override
    public BJInfo getItem(int i) {
        return mBJList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = mInflater.inflate(R.layout.bj_list_row, null);
            holder = new ViewHolder();
            holder.bjThumbnail = (CircleNetworkImageView) view.findViewById(R.id.bjThumbnail);
            holder.bjName = (TextView) view.findViewById(R.id.video_title);
            holder.intro = (TextView) view.findViewById(R.id.intro);
            holder.hits = (TextView) view.findViewById(R.id.hits);
            holder.favorite = (ImageView) view.findViewById(R.id.favorite);
            holder.partnerBJRing = (ImageView) view.findViewById(R.id.partnerBJRing);
            holder.partnerBJSmallCircle = (ImageView) view.findViewById(R.id.partnerBJSmallCircle);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.position = i;
        view.setTag(holder);


        holder.bjThumbnail.loadCircleBitmap(getItem(i).getThumnailurl());
        holder.bjName.setText(getItem(i).getBjName());
        holder.intro.setText(getItem(i).getbjsogae().equals("") ? mActivity.getText(R.string.str_empty_bj_desc) : getItem(i).getbjsogae());
        holder.partnerBJRing.setVisibility(getItem(i).getPartnertype().equals(TYPE_PARTNER) ? View.VISIBLE : View.INVISIBLE);
        holder.partnerBJSmallCircle.setVisibility(getItem(i).getPartnertype().equals(TYPE_PARTNER) ? View.VISIBLE : View.INVISIBLE);

        try {
            if (Integer.parseInt(getItem(i).getbjsubscriberCount()) <= 0) {
            } else {
                holder.hits.setText(NumberFormat.getInstance().format(Integer.parseInt(getItem(i).getbjsubscriberCount())));
            }
        } catch (Exception e) {
            e.printStackTrace();
            holder.hits.setText("?");
        }

        if (getItem(i).isFavorite()) {
            holder.favorite.setImageResource(R.drawable.bookmark_btn_on);
        } else {
            holder.favorite.setImageResource(R.drawable.bookmark_btn_off);
        }
        holder.favorite.setOnClickListener(mBookMarkClickListener);

        return view;
    }

    private void sendBookMarkBroadcast(String bookMarkActionType, String bookMarkContent, String id) {
        Intent intent = new Intent(ACTION_BOOKMARK_CHANGED);
        intent.putExtra(EXTRA_BOOKMARK_CONTENT_TYPE, bookMarkContent);
        intent.putExtra(EXTRA_BOOKMARK_ACTION_TYPE, bookMarkActionType);
        intent.putExtra(EXTRA_BOOKMARK_ID, id);
        mActivity.sendBroadcast(intent);
    }

    private final View.OnClickListener mBookMarkClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            View parent = (View) v.getParent().getParent();
            ViewHolder holder = (ViewHolder) parent.getTag();
            BJInfo item = mBJList.get(holder.position);
            LogUtil.LOGD("BJLIstAdapter :: mFavoriteClickListener :: position = " + holder.position);
            if (getItem(holder.position).isFavorite()) {
                Toast.makeText(mActivity, R.string.str_favorite_delete, Toast.LENGTH_SHORT).show();
                mDataManager.deleteFavoriteBjInfo(getItem(holder.position).getBJChannelID());
                holder.favorite.setImageResource(R.drawable.bookmark_btn_off);
                item.setFavorite(false);
                sendBookMarkBroadcast(BOOKMARK_ACTION_TYPE_DELETE, BOOKMARK_CONTENT_TYPE_BJ, getItem(holder.position).getBJChannelID());
            } else {
                mDataManager.insertFavoriteBJ(getItem(holder.position));
                Toast.makeText(mActivity, R.string.str_favorite_add, Toast.LENGTH_SHORT).show();
                holder.favorite.setImageResource(R.drawable.bookmark_btn_on);
                item.setFavorite(true);
                sendBookMarkBroadcast(BOOKMARK_ACTION_TYPE_ADD, BOOKMARK_CONTENT_TYPE_BJ, getItem(holder.position).getBJChannelID());
            }
            mBJList.set(holder.position, item);
        }
    };

    class ViewHolder {
        public CircleNetworkImageView bjThumbnail;
        public TextView bjName;
        public TextView intro;
        public TextView hits;
        public ImageView favorite;
        public ImageView partnerBJRing;
        public ImageView partnerBJSmallCircle;
        public int position;
    }
}
