package com.loginsoft.moabj.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.loginsoft.moabj.R;
import com.loginsoft.moabj.Util.LogUtil;

/**
 * Created by Administrator on 2015-02-07.
 */
public class MoaBJMainTabView extends LinearLayout {

    private ImageView mTabHome;
    private ImageView mTabTopTwenty;
    private ImageView mTabBJ;
    private ImageView mTabBookmark;
    private ImageView mTabSetting;

    private MoaBJMainTabListener mTabListener;

    private int mSelectedTab = 0;

    public interface MoaBJMainTabListener {
        void onTabSelected(int position);
    }


    public MoaBJMainTabView(Context context) {
        this(context, null);
    }

    public MoaBJMainTabView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mTabHome = (ImageView) findViewById(R.id.tab_home);
        mTabTopTwenty = (ImageView) findViewById(R.id.tab_top);
        mTabBJ = (ImageView) findViewById(R.id.tab_bj);
        mTabBookmark = (ImageView) findViewById(R.id.tab_bookmark);
        mTabSetting = (ImageView) findViewById(R.id.tab_setting);

        mTabHome.setOnClickListener(mTabClickListener);
        mTabTopTwenty.setOnClickListener(mTabClickListener);
        mTabBJ.setOnClickListener(mTabClickListener);
        mTabBookmark.setOnClickListener(mTabClickListener);
        mTabSetting.setOnClickListener(mTabClickListener);

        mTabHome.setSelected(true);
    }

    public void setMoaBJMainTabListener(MoaBJMainTabListener listener) {
        mTabListener = listener;
    }

    public void setSelectTab(int position) {
        LogUtil.LOGD("MoaBJMainTabView :: setSelectTab() :: position = " + position + " mSelectTab = " + mSelectedTab);
        LogUtil.LOGD("MoaBJMainTabView :: setSelectTab() :: childCount = " + getChildCount());
        if (position != mSelectedTab) {
            getChildAt(mSelectedTab).setSelected(false);
            getChildAt(position).setSelected(true);
            mSelectedTab = position;
        }
    }

    private final OnClickListener mTabClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            int selectPosition = 0;
            switch (v.getId()) {
                case R.id.tab_home:
                    selectPosition = 0;
                    break;

                case R.id.tab_top:
                    selectPosition = 1;
                    break;

                case R.id.tab_bj:
                    selectPosition = 2;
                    break;

                case R.id.tab_bookmark:
                    selectPosition = 3;
                    break;

                case R.id.tab_setting:
                    selectPosition = 4;
                    break;
            }

            if (mTabListener != null) {
                if (mSelectedTab != selectPosition) {
                    v.setSelected(true);
                    getChildAt(mSelectedTab).setSelected(false);
                    mSelectedTab = selectPosition;
                    mTabListener.onTabSelected(selectPosition);
                }
            }
        }
    };
}
