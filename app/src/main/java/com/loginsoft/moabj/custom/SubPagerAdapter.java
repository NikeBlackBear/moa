package com.loginsoft.moabj.custom;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.loginsoft.moabj.common.CommonValues;
import com.loginsoft.moabj.main.MoaBJTopSubFragment;
import com.loginsoft.moabj.model.HotVideo;

import java.util.ArrayList;

/**
 * Created by Administrator on 2015-02-02.
 */
public class SubPagerAdapter extends FragmentStatePagerAdapter implements CommonValues {
    private ArrayList<HotVideo> mHotVideoList = new ArrayList<HotVideo>();
    private ArrayList<MoaBJTopSubFragment> mSubFragmentList;

    public SubPagerAdapter(FragmentManager fm, ArrayList<HotVideo> hotVideoList) {
        super(fm);
        mHotVideoList = hotVideoList;
        mSubFragmentList = new ArrayList<MoaBJTopSubFragment>();
        for (int i = 0; i < 5; i++) {
            mSubFragmentList.add(new MoaBJTopSubFragment());
        }
    }

    @Override
    public Fragment getItem(int position) {
        Bundle dataBox = new Bundle();
        if (mHotVideoList.size() > 0) {
            dataBox.putSerializable(EXTRA_HOT_VIDEO, mHotVideoList.get(position));
            dataBox.putInt(EXTRA_SUB_POSITION, position);
        }

        MoaBJTopSubFragment subFragment = mSubFragmentList.get(position);
        subFragment.setArguments(dataBox);
        return subFragment;
    }

    @Override
    public int getCount() {
        return mSubFragmentList.size();
    }
}
