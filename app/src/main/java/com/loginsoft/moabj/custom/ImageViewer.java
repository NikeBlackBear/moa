package com.loginsoft.moabj.custom;

import android.content.Context;
import android.util.AttributeSet;

import com.android.volley.toolbox.NetworkImageView;
import com.loginsoft.moabj.R;
import com.loginsoft.moabj.http.ServerProxy;

/**
 * Created by Administrator on 2015-02-04.
 */
public class ImageViewer extends NetworkImageView {
    private Context mContext;

    public ImageViewer(Context context) {
        this(context, null, 0);
    }

    public ImageViewer(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ImageViewer(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        setDefaultImageResId(R.drawable.imageload);
    }

    public void setImageURL(String Url) {
        setImageUrl(Url, ServerProxy.getInstance(mContext).getImageLoader());
    }
}
