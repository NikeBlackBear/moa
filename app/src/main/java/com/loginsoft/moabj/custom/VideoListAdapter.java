package com.loginsoft.moabj.custom;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.loginsoft.moabj.R;
import com.loginsoft.moabj.Util.DateTime;
import com.loginsoft.moabj.common.CommonValues;
import com.loginsoft.moabj.database.DataManager;
import com.loginsoft.moabj.main.MoaBJDetailBJActivity;
import com.loginsoft.moabj.main.MoaBJMainFrame;
import com.loginsoft.moabj.model.BJInfo;
import com.loginsoft.moabj.model.HotVideo;

import java.text.NumberFormat;
import java.util.List;

/**
 * Created by Administrator on 2015-02-05.
 */
public class VideoListAdapter extends BaseAdapter implements CommonValues {
    private Activity mActivity;
    private List<HotVideo> mBJVideoList;
    private LayoutInflater mInflater;
    private DataManager mDataManager;
    private BJInfo mTargetBJInfo;

    public VideoListAdapter(Activity activity, List<HotVideo> list, BJInfo bjInfo) {
        mActivity = activity;
        mInflater = LayoutInflater.from(activity);
        mBJVideoList = list;
        mTargetBJInfo = bjInfo;
        if (activity instanceof MoaBJMainFrame) {
            mDataManager = ((MoaBJMainFrame) activity).getDataManager();
        } else if (activity instanceof MoaBJDetailBJActivity) {
            mDataManager = ((MoaBJDetailBJActivity) activity).getDataManager();
        }
    }

    @Override
    public int getCount() {
        return mBJVideoList.size();
    }

    @Override
    public HotVideo getItem(int i) {
        return mBJVideoList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = mInflater.inflate(R.layout.video_list_row, null);
            holder = new ViewHolder();
            holder.videoThumbnail = (ImageViewer) view.findViewById(R.id.video_thumbnail);
            holder.videoTime = (TypefaceTextView) view.findViewById(R.id.video_time);
            holder.videoTitle = (TypefaceTextView) view.findViewById(R.id.video_title);
            holder.bjName = (TypefaceTextView) view.findViewById(R.id.bj_name);
            holder.uploadDate = (TypefaceTextView) view.findViewById(R.id.upload_date);
            holder.viewCount = (TypefaceTextView) view.findViewById(R.id.view_count);
            holder.favorite = (ImageView) view.findViewById(R.id.bookmark_btn);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.position = i;
        view.setTag(holder);

        holder.videoThumbnail.setImageURL(getItem(i).getVideoThumbnail());
        holder.videoTime.setText(getItem(i).getDuration());
        holder.videoTitle.setText(getItem(i).getTitle());
        holder.bjName.setText(mTargetBJInfo.getBjName());
        holder.uploadDate.setText(DateTime.getUploadDateCompareCurrentDate(mActivity, getItem(i).getUploadDate()));
        try {
            holder.viewCount.setText(NumberFormat.getInstance().format(Integer.parseInt(getItem(i).getViewCount())));
        } catch (Exception e) {
            e.printStackTrace();
            holder.viewCount.setText("?");
        }
        if (getItem(i).isFavorite()) {
            holder.favorite.setImageResource(R.drawable.bookmark_btn_on);
        } else {
            holder.favorite.setImageResource(R.drawable.bookmark_btn_off);
        }
        holder.favorite.setOnClickListener(mFavoriteClickListener);
        return view;
    }

    private void sendBookMarkBroadcast(String bookMarkActionType, String bookMarkContent, String id) {
        Intent intent = new Intent(ACTION_BOOKMARK_CHANGED);
        intent.putExtra(EXTRA_BOOKMARK_CONTENT_TYPE, bookMarkContent);
        intent.putExtra(EXTRA_BOOKMARK_ACTION_TYPE, bookMarkActionType);
        intent.putExtra(EXTRA_BOOKMARK_ID, id);
        mActivity.sendBroadcast(intent);
    }

    private final View.OnClickListener mFavoriteClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            View parent = (View) v.getParent().getParent().getParent();
            ViewHolder holder = (ViewHolder) parent.getTag();
            HotVideo item = getItem(holder.position);
            if (getItem(holder.position).isFavorite()) {
                Toast.makeText(mActivity, R.string.str_favorite_delete, Toast.LENGTH_SHORT).show();
                mDataManager.deleteFavoriteVideoInfo(getItem(holder.position).getVideoID());
                holder.favorite.setImageResource(R.drawable.bookmark_btn_off);
                item.setFavorite(false);
                sendBookMarkBroadcast(BOOKMARK_ACTION_TYPE_DELETE, BOOKMARK_CONTENT_TYPE_VIDEO, getItem(holder.position).getVideoID());
            } else {
                item.setBJIcon(mTargetBJInfo.getThumnailurl());
                mDataManager.insertFavoriteVideoData(getItem(holder.position));
                Toast.makeText(mActivity, R.string.str_favorite_add, Toast.LENGTH_SHORT).show();
                holder.favorite.setImageResource(R.drawable.bookmark_btn_on);
                item.setFavorite(true);
                sendBookMarkBroadcast(BOOKMARK_ACTION_TYPE_ADD, BOOKMARK_CONTENT_TYPE_VIDEO, getItem(holder.position).getVideoID());
            }
            mBJVideoList.set(holder.position, item);
        }
    };

    class ViewHolder {
        public ImageViewer videoThumbnail;
        public TypefaceTextView videoTime;
        public TypefaceTextView videoTitle;
        public TypefaceTextView bjName;
        public TypefaceTextView uploadDate;
        public TypefaceTextView viewCount;
        public ImageView favorite;
        public int position;
    }
}