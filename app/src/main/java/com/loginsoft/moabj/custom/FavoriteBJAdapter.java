package com.loginsoft.moabj.custom;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.loginsoft.moabj.R;
import com.loginsoft.moabj.database.DataManager;
import com.loginsoft.moabj.main.MoaBJDetailBJActivity;
import com.loginsoft.moabj.main.MoaBJMainFrame;
import com.loginsoft.moabj.model.BJInfo;

import java.util.List;

/**
 * Created by Administrator on 2015-02-08.
 */
public class FavoriteBJAdapter extends BaseAdapter {
    private Activity mActivity;
    private List<BJInfo> mBjList;
    private LayoutInflater mInflater;
    private DataManager mDataManager;
    private OnDeleteBookMarkListener mDeleteBookMarkListener;

    public FavoriteBJAdapter(Activity activity, List<BJInfo> list) {
        mActivity = activity;
        mInflater = LayoutInflater.from(activity);
        mBjList = list;
        if (activity instanceof MoaBJMainFrame) {
            mDataManager = ((MoaBJMainFrame) activity).getDataManager();
        } else if (activity instanceof MoaBJDetailBJActivity) {
            mDataManager = ((MoaBJDetailBJActivity) activity).getDataManager();
        }
    }

    public void setOnDeleteBookMarkListener(OnDeleteBookMarkListener listener) {
        mDeleteBookMarkListener = listener;
    }

    @Override
    public int getCount() {
        return mBjList.size();
    }

    @Override
    public BJInfo getItem(int position) {
        return mBjList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = mInflater.inflate(R.layout.favorite_bj_cell, null);
            holder = new ViewHolder();
            holder.bjname = (TypefaceTextView) view.findViewById(R.id.video_title);
            holder.bjThumnail = (CircleNetworkImageView) view.findViewById(R.id.bjThumbnail);
            holder.favorite = (ImageView) view.findViewById(R.id.favorite);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.position = i;
        view.setTag(holder);

        holder.bjThumnail.loadCircleBitmap(getItem(i).getThumnailurl());
        holder.bjname.setText(getItem(i).getBjName());
        holder.favorite.setOnClickListener(checkoutFavorite);
        return view;
    }

    private final View.OnClickListener checkoutFavorite = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            View parent = (View) v.getParent().getParent();
            ViewHolder holder = (ViewHolder) parent.getTag();
            mDataManager.deleteFavoriteBjInfo(getItem(holder.position).getBJChannelID());
            if (mDeleteBookMarkListener != null) {
                mDeleteBookMarkListener.onDelete(holder.position, getItem(holder.position));
            }
        }
    };

    public class ViewHolder {
        public CircleNetworkImageView bjThumnail;
        public ImageView favorite;
        public TypefaceTextView bjname;
        public int position;
    }
}
