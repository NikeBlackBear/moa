package com.loginsoft.moabj.database;

/**
 * Created by Administrator on 2015-02-05.
 */
public interface DBValues {
    public static final String TABLE_FAVORITE_VIDEO_INFO = "table_favorite_video_info";
    public static final String TABEL_FAVORITE_BJ_INFO = "table_favorite_bj_info";

    public static final String COLUMN_VIDEO_PK = "video_pk";
    public static final String COLUMN_VIDEO_ID = "video_id";
    public static final String COLUMN_VIDEO_IMAGE_URL = "video_image_url";
    public static final String COLUMN_VIDEO_TITLE = "video_title";
    public static final String COLUMN_VIDEO_VIEWCOUNT = "video_viewcount";
    public static final String COLUMN_VIDEO_DURATION = "video_duration";
    public static final String COLUMN_VIDEO_CHANNELTITLE = "video_channeltitle";
    public static final String COLUMN_VIDEO_BJNAME = "video_bjname";
    public static final String COLUMN_VIDEO_BJICON = "video_bjicon";
    public static final String COLUMN_VIDEO_UPLOADDATE = "video_uploaddate";

    public static final String COLUMN_BJ_PK = "bj_pk";
    public static final String COLUMN_BJ_CHANELID = "bj_chanelid";
    public static final String COLUMN_BJ_NAME = "bj_name";
    public static final String COLUMN_BJ_IMAGEURL = "bj_imageurl";
    public static final String COLUMN_BJ_SOGAE = "bj_sogae";
    public static final String COLUMN_BJ_SUBSCRIBERCOUNT = "bj_subscribercount";
    public static final String COLUMN_BJ_VIEWCOUNT = "bj_viewcount";
    public static final String COLUMN_BJ_AFRICA_URL = "bj_africa_url";
    public static final String COLUMN_BJ_BANNERURL = "bj_bannerid";
    public static final String COLUMN_BJ_PARTNERTYPE = "bj_partnertype";
    public static final String COLUMN_BJ_TYPE = "bj_type";
}
