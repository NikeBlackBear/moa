package com.loginsoft.moabj.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.loginsoft.moabj.Util.LogUtil;
import com.loginsoft.moabj.common.CommonValues;
import com.loginsoft.moabj.model.BJInfo;
import com.loginsoft.moabj.model.HotVideo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015-02-05.
 */
public class DataManager implements CommonValues, DBValues {
    private FavoriteVideoDBHelper mFavoriteVideoDBHelper;
    private BJInfoDBHelper mFavoriteBJDBHelper;
    private SQLiteDatabase mFavoriteVideoDatabase;
    private SQLiteDatabase mFavoriteBJDatabase;


    public DataManager(Context context) {
        mFavoriteVideoDBHelper = new FavoriteVideoDBHelper(context);
        mFavoriteBJDBHelper = new BJInfoDBHelper(context);
        mFavoriteVideoDatabase = mFavoriteVideoDBHelper.getWritableDatabase();
        mFavoriteBJDatabase = mFavoriteBJDBHelper.getWritableDatabase();
    }

    public void close() {
        mFavoriteVideoDBHelper.close();
        mFavoriteBJDBHelper.close();
        mFavoriteBJDatabase.close();
        mFavoriteVideoDatabase.close();
    }

    // 동영상 즐겨찾기 추가하는 메소드 (내부 DB에 저장)
    public void insertFavoriteVideoData(HotVideo favoriteVideo) {
        LogUtil.LOGE("Data Manager :: insertFavoriteVideoData ::  favoriteVideo = " + favoriteVideo.toString());
        ContentValues values = new ContentValues();
        values.put(COLUMN_VIDEO_ID, favoriteVideo.getVideoID());
        values.put(COLUMN_VIDEO_IMAGE_URL, favoriteVideo.getVideoThumbnail());
        values.put(COLUMN_VIDEO_TITLE, favoriteVideo.getTitle());
        values.put(COLUMN_VIDEO_DURATION, favoriteVideo.getDuration());
        values.put(COLUMN_VIDEO_CHANNELTITLE, favoriteVideo.getChannelTitle());
        values.put(COLUMN_VIDEO_UPLOADDATE, favoriteVideo.getUploadDate());
        values.put(COLUMN_VIDEO_VIEWCOUNT, favoriteVideo.getViewCount());
        values.put(COLUMN_VIDEO_BJICON, favoriteVideo.getBJIcon());
        mFavoriteVideoDatabase.insert(TABLE_FAVORITE_VIDEO_INFO, null, values);

    }

    public HotVideo getTargetFavoriteVideoData(String videoID) {
        String where = COLUMN_VIDEO_ID + " = '" + videoID + "'";
        Cursor cursor = mFavoriteVideoDatabase.query(TABLE_FAVORITE_VIDEO_INFO, null, where, null, null, null, null);
        if (cursor != null) {
            LogUtil.LOGE("DataManager :: getTargetFavoriteVideoData :: Cursor Cout = " + cursor.getCount());
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                HotVideo favorite = new HotVideo();
                LogUtil.LOGE("id = " + cursor.getLong(cursor.getColumnIndex(COLUMN_VIDEO_PK)));
                favorite.setVideoID(cursor.getString(cursor.getColumnIndex(COLUMN_VIDEO_ID)));
                favorite.setVideoThumbnail(cursor.getString(cursor.getColumnIndex(COLUMN_VIDEO_IMAGE_URL)));
                favorite.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_VIDEO_TITLE)));
                favorite.setViewCount(cursor.getString(cursor.getColumnIndex(COLUMN_VIDEO_VIEWCOUNT)));
                favorite.setDuration(cursor.getString(cursor.getColumnIndex(COLUMN_VIDEO_DURATION)));
                favorite.setChannelTitle(cursor.getString(cursor.getColumnIndex(COLUMN_VIDEO_CHANNELTITLE)));
                favorite.setUploadDate(cursor.getString(cursor.getColumnIndex(COLUMN_VIDEO_UPLOADDATE)));
                favorite.setBJIcon(cursor.getString(cursor.getColumnIndex(COLUMN_VIDEO_BJICON)));
                cursor.close();
                return favorite;
            } else {
                cursor.close();
                return null;
            }
        } else {
            LogUtil.LOGE("DataManager :: getTargetFavoriteVideoData :: Cursor Count is null");
            cursor.close();
            return null;
        }
    }

    //즐겨찾기 추가된 영상 정보 불러오는 메소드 (내부 디비에서 불러오기)
    public List<HotVideo> getAllFavoriteVideoData() {
        List<HotVideo> favoritelist = new ArrayList<>();
        Cursor cursor = mFavoriteVideoDatabase.query(TABLE_FAVORITE_VIDEO_INFO, null, null, null, null, null, null);
        if (cursor != null) {
            LogUtil.LOGE("DataManager :: getAllFavoriteVideoData :: Cursor Cout = " + cursor.getCount());
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    HotVideo favorite = new HotVideo();
                    LogUtil.LOGE("id = " + cursor.getLong(cursor.getColumnIndex(COLUMN_VIDEO_PK)));
                    favorite.setVideoID(cursor.getString(cursor.getColumnIndex(COLUMN_VIDEO_ID)));
                    favorite.setVideoThumbnail(cursor.getString(cursor.getColumnIndex(COLUMN_VIDEO_IMAGE_URL)));
                    favorite.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_VIDEO_TITLE)));
                    favorite.setViewCount(cursor.getString(cursor.getColumnIndex(COLUMN_VIDEO_VIEWCOUNT)));
                    favorite.setDuration(cursor.getString(cursor.getColumnIndex(COLUMN_VIDEO_DURATION)));
                    favorite.setChannelTitle(cursor.getString(cursor.getColumnIndex(COLUMN_VIDEO_CHANNELTITLE)));
                    favorite.setUploadDate(cursor.getString(cursor.getColumnIndex(COLUMN_VIDEO_UPLOADDATE)));
                    favorite.setBJIcon(cursor.getString(cursor.getColumnIndex(COLUMN_VIDEO_BJICON)));
                    favoritelist.add(favorite);
                } while (cursor.moveToNext());
                cursor.close();
            } else {
                cursor.close();
            }
        } else {
            LogUtil.LOGE("DataManager :: getAllFavoriteVideoData :: Cursor Count is null");
            cursor.close();
        }
        for (int i = 0; i < favoritelist.size(); i++) {
            LogUtil.LOGE("LSJ :: " + favoritelist.get(i).toString());
        }
        return favoritelist;
    }

    //BJ 즐겨찾기 추가 하는 메소드 (내부 DB에 저장)
    public void insertFavoriteBJ(BJInfo bjinfo) {
        LogUtil.LOGE("Data Manager :: insertFavoriteBJ ::  favoritebjinfo = " + bjinfo.toString());
        ContentValues values = new ContentValues();
        values.put(COLUMN_BJ_CHANELID, bjinfo.getBJChannelID());
        values.put(COLUMN_BJ_IMAGEURL, bjinfo.getThumnailurl());
        values.put(COLUMN_BJ_NAME, bjinfo.getBjName());
        values.put(COLUMN_BJ_SOGAE, bjinfo.getbjsogae());
        values.put(COLUMN_BJ_SUBSCRIBERCOUNT, bjinfo.getbjsubscriberCount());
        values.put(COLUMN_BJ_VIEWCOUNT, bjinfo.getViewCount());
        values.put(COLUMN_BJ_AFRICA_URL, bjinfo.getAfracaURL());
        values.put(COLUMN_BJ_BANNERURL, bjinfo.getBJBannerURL());
        values.put(COLUMN_BJ_PARTNERTYPE, bjinfo.getPartnertype());
        values.put(COLUMN_BJ_TYPE, bjinfo.getType());
        mFavoriteBJDatabase.insert(TABEL_FAVORITE_BJ_INFO, null, values);
    }

    //즐겨찾기 추가한 BJ의 데이터를 불러오는 메소드
    public BJInfo getTargetFavoriteBJData(String bjChannelID) {
        String where = COLUMN_BJ_CHANELID + " = '" + bjChannelID + "'";
        Cursor cursor = mFavoriteBJDatabase.query(TABEL_FAVORITE_BJ_INFO, null, where, null, null, null, null);
        if (cursor != null) {
            LogUtil.LOGE("DataManager :: getTargetFavoriteBJData :: Cursor Cout = " + cursor.getCount());
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                BJInfo favorite = new BJInfo();
                favorite.setBJChannelID(cursor.getString(cursor.getColumnIndex(COLUMN_BJ_CHANELID)));
                favorite.setBjName(cursor.getString(cursor.getColumnIndex(COLUMN_BJ_NAME)));
                favorite.setThumnailURL(cursor.getString(cursor.getColumnIndex(COLUMN_BJ_IMAGEURL)));
                favorite.setbjsogae(cursor.getString(cursor.getColumnIndex(COLUMN_BJ_SOGAE)));
                favorite.setbjsubscriberCount(cursor.getString(cursor.getColumnIndex(COLUMN_BJ_SUBSCRIBERCOUNT)));
                favorite.setViewCount(cursor.getString(cursor.getColumnIndex(COLUMN_BJ_VIEWCOUNT)));
                favorite.setAfracaURL(cursor.getString(cursor.getColumnIndex(COLUMN_BJ_AFRICA_URL)));
                favorite.setBJBannerURL(cursor.getString(cursor.getColumnIndex(COLUMN_BJ_BANNERURL)));
                favorite.setPartnertype(cursor.getString(cursor.getColumnIndex(COLUMN_BJ_PARTNERTYPE)));
                favorite.setType(cursor.getString(cursor.getColumnIndex(COLUMN_BJ_TYPE)));
                favorite.setFavorite(true);
                cursor.close();
                return favorite;
            } else {
                cursor.close();
                return null;
            }
        } else {
            LogUtil.LOGE("DataManager :: getTargetFavoriteBJData :: Cursor Count is null");
            cursor.close();
            return null;
        }
    }

    //즐겨찾기 추가된 BJ 정보 불러오는 메소드 (내부 디비에서 불러오기)
    public List<BJInfo> getAllFavoriteBJData() {
        List<BJInfo> favoritelist = new ArrayList<>();
        Cursor cursor = mFavoriteBJDatabase.query(TABEL_FAVORITE_BJ_INFO, null, null, null, null, null, null);
        if (cursor != null) {
            LogUtil.LOGE("DataManager :: getAllFavoriteBJData :: Cursor Cout = " + cursor.getCount());
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    BJInfo favorite = new BJInfo();
                    favorite.setBJChannelID(cursor.getString(cursor.getColumnIndex(COLUMN_BJ_CHANELID)));
                    favorite.setBjName(cursor.getString(cursor.getColumnIndex(COLUMN_BJ_NAME)));
                    favorite.setThumnailURL(cursor.getString(cursor.getColumnIndex(COLUMN_BJ_IMAGEURL)));
                    favorite.setbjsogae(cursor.getString(cursor.getColumnIndex(COLUMN_BJ_SOGAE)));
                    favorite.setbjsubscriberCount(cursor.getString(cursor.getColumnIndex(COLUMN_BJ_SUBSCRIBERCOUNT)));
                    favorite.setViewCount(cursor.getString(cursor.getColumnIndex(COLUMN_BJ_VIEWCOUNT)));
                    favorite.setAfracaURL(cursor.getString(cursor.getColumnIndex(COLUMN_BJ_AFRICA_URL)));
                    favorite.setBJBannerURL(cursor.getString(cursor.getColumnIndex(COLUMN_BJ_BANNERURL)));
                    favorite.setPartnertype(cursor.getString(cursor.getColumnIndex(COLUMN_BJ_PARTNERTYPE)));
                    favorite.setType(cursor.getString(cursor.getColumnIndex(COLUMN_BJ_TYPE)));
                    favorite.setFavorite(true);
                    favoritelist.add(favorite);
                } while (cursor.moveToNext());
                cursor.close();
            } else {
                cursor.close();
            }
        } else {
            LogUtil.LOGE("DataManager :: getAllFavoriteBJData :: Cursor Count is null");
            cursor.close();
        }
        for (int i = 0; i < favoritelist.size(); i++) {
            LogUtil.LOGE("DATA111 :: " + favoritelist.get(i).toString());
        }
        return favoritelist;
    }

    //BJ 즐겨찾기 해제 메소드
    public void deleteFavoriteBjInfo(String bjChannelID) {
        String where = COLUMN_BJ_CHANELID + " = '" + bjChannelID + "'";
        mFavoriteBJDatabase.delete(TABEL_FAVORITE_BJ_INFO, where, null);
    }

    //Video 즐겨찾기 해제 메소드
    public void deleteFavoriteVideoInfo(String videoID) {
        String where = COLUMN_VIDEO_ID + " = '" + videoID + "'";
        mFavoriteVideoDatabase.delete(TABLE_FAVORITE_VIDEO_INFO, where, null);

    }

    //즐겨찾기 된 Video 체크 메소드
    public boolean isAlreadyFavoriteVideo(String videoID) {
        String where = COLUMN_VIDEO_ID + " = '" + videoID + "'";
        Cursor cursor = mFavoriteVideoDatabase.query(TABLE_FAVORITE_VIDEO_INFO, null, where, null, null, null, null, null);
        if (cursor.getCount() > 0) {
            return true;
        }
        return false;
    }

    //즐겨찾기 된 BJ 체크 메소드
    public boolean isAlreadyFavoriteBJ(String bjChannelID) {
        String where = COLUMN_BJ_CHANELID + " = '" + bjChannelID + "'";
        Cursor cursor = mFavoriteBJDatabase.query(TABEL_FAVORITE_BJ_INFO, null, where, null, null, null, null, null);
        if (cursor.getCount() > 0) {
            return true;
        }
        return false;
    }

    //BJ 즐겨찾기 데이터 유무
    public boolean isExistBJFavoriteData() {
        Cursor cursor = mFavoriteBJDatabase.query(TABEL_FAVORITE_BJ_INFO, null, null, null, null, null, null, null);
        if (cursor.getCount() > 0) {
            return true;
        }
        return false;
    }

    //Video 즐겨찾기 데이터 유무
    public boolean isExistVideoFavoriteData() {
        Cursor cursor = mFavoriteVideoDatabase.query(TABLE_FAVORITE_VIDEO_INFO, null, null, null, null, null, null, null);
        if (cursor.getCount() > 0) {
            return true;
        }
        return false;
    }
}