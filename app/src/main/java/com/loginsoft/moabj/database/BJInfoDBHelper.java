package com.loginsoft.moabj.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Administrator on 2015-02-06.
 */
public class BJInfoDBHelper extends SQLiteOpenHelper implements DBValues {
    private String createSql = "create table " + TABEL_FAVORITE_BJ_INFO + " ("
            + COLUMN_BJ_PK + " integer primary key autoincrement, "
            + COLUMN_BJ_CHANELID + " text, "
            + COLUMN_BJ_NAME + " text, "
            + COLUMN_BJ_IMAGEURL + " text, "
            + COLUMN_BJ_SOGAE + " text, "
            + COLUMN_BJ_SUBSCRIBERCOUNT + " text, "
            + COLUMN_BJ_VIEWCOUNT + " text, "
            + COLUMN_BJ_AFRICA_URL + " text, "
            + COLUMN_BJ_BANNERURL + " text, "
            + COLUMN_BJ_PARTNERTYPE + " text, "
            + COLUMN_BJ_TYPE + " text)";

    public BJInfoDBHelper(Context context) {
        super(context, "BJInfoDBHelper", null, 2);
    }


    @Override
    public void onCreate(SQLiteDatabase arg0) {
        arg0.execSQL(createSql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        switch (oldVersion) {
            case 1:
                db.execSQL("drop table if exists " + TABEL_FAVORITE_BJ_INFO);
                db.execSQL(createSql);
//                db.execSQL("alter table " + TABEL_FAVORITE_BJ_INFO + " add column " + COLUMN_BJ_PARTNERTYPE + " text default 'N'");
//                db.execSQL("alter table " + TABEL_FAVORITE_BJ_INFO + " add column " + COLUMN_BJ_TYPE + " text default 'hasPlayList'");
                break;
        }
    }
}
