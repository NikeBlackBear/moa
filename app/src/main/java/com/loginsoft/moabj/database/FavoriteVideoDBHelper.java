package com.loginsoft.moabj.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Administrator on 2015-02-05.
 */
public class FavoriteVideoDBHelper extends SQLiteOpenHelper implements DBValues {
    public FavoriteVideoDBHelper(Context context) {
        super(context, "FavoriteVideo_Database", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase arg0) {
        String createSql = "create table " + TABLE_FAVORITE_VIDEO_INFO + " ("
                + COLUMN_VIDEO_PK + " integer primary key autoincrement, "
                + COLUMN_VIDEO_ID + " text, "
                + COLUMN_VIDEO_IMAGE_URL + " text, "
                + COLUMN_VIDEO_TITLE + " text, "
                + COLUMN_VIDEO_VIEWCOUNT + " test, "
                + COLUMN_VIDEO_DURATION + " text, "
                + COLUMN_VIDEO_CHANNELTITLE + " text, "
                + COLUMN_VIDEO_BJNAME + " text, "
                + COLUMN_VIDEO_BJICON + " text, "
                + COLUMN_VIDEO_UPLOADDATE + " text)";
        arg0.execSQL(createSql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}
