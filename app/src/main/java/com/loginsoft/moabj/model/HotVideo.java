package com.loginsoft.moabj.model;

import java.io.Serializable;

/**
 * Created by Administrator on 2015-02-04.
 */
public class HotVideo implements Serializable {
    private String title;
    private String viewcount;
    private String channeltitle;
    private String duration;
    private String uploaddate;
    private String image;
    private String bjname;
    private String videoID;
    private String bjicon;
    private String nextpagetoken;
    private String totalresult;
    private boolean isFavorite = false;

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setViewCount(String viewcount) {
        this.viewcount = viewcount;
    }

    public String getViewCount() {
        return viewcount;
    }

    public void setChannelTitle(String channeltitle) {
        this.channeltitle = channeltitle;
    }

    public String getChannelTitle() {
        return channeltitle;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDuration() {
        return duration;
    }

    public void setUploadDate(String date) {
        uploaddate = date;
    }

    public String getUploadDate() {
        return uploaddate;
    }

    public void setVideoThumbnail(String image) {
        this.image = image;
    }

    public String getVideoThumbnail() {
        return image;
    }

    public void setbjname(String bjname2) {
        this.bjname = bjname2;
    }

    public String getbjname() {
        return bjname;
    }

    public void setVideoID(String videoID) {
        this.videoID = videoID;
    }

    public String getVideoID() {
        return videoID;
    }

    public void setBJIcon(String bjiconurl) {
        this.bjicon = bjiconurl;
    }

    public String getBJIcon() {
        return bjicon;
    }

    public void setNextPageToken(String token) {
        nextpagetoken = token;
    }

    public String getNextpagetoken() {
        return nextpagetoken;
    }

    public void setTotalResult(String result) {
        totalresult = result;
    }

    public String getTotalResult() {
        return totalresult;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    @Override
    public String toString() {
        return "[title - " + title + "] [viewcount - " + viewcount + "] [channeltitle - " + channeltitle + "] " +
                "[duration - " + duration + "] [uploaddate - " + uploaddate + "] [bjname - " + bjname + "] " +
                "[ image - " + image + "] [bjicon - " + bjicon + "] ";
    }
}
