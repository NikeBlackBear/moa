package com.loginsoft.moabj.model;

import java.io.Serializable;

/**
 * Created by Administrator on 2015-02-04.
 */
public class BJInfo implements Serializable {
    private String bjname;                   // BJ 이름
    private String bjsogae;                  //BJ 소개글
    private String bjviewcount;
    private String bjsubscriberCount;         // 구독자수
    private boolean favorite = false;                // 즐겨찾기
    private String bjchannelID;             // BJ 채널ID
    private String bjbannerurl;             // BJ 배너 URL
    private String thumnailurl;             // BJ 이미지
    private String africaurl;
    private String partnertype;             // 제휴 BJ
    private String type;                    // channel ID 유무 확인

    public void setBjName(String bjname) {
        this.bjname = bjname;
    }

    public String getBjName() {
        return bjname;
    }

    public void setbjsogae(String head) {
        this.bjsogae = head;
    }

    public String getbjsogae() {
        return bjsogae;
    }

    public void setViewCount(String viewCount) {
        this.bjviewcount = viewCount;
    }

    public String getViewCount() {
        return bjviewcount;
    }

    public void setbjsubscriberCount(String subScription) {
        this.bjsubscriberCount = subScription;
    }

    public String getbjsubscriberCount() {
        return bjsubscriberCount;
    }

    public void setFavorite(boolean sf) {
        this.favorite = sf;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setBJChannelID(String bjchannelID) {
        this.bjchannelID = bjchannelID;
    }

    public String getBJChannelID() {
        return bjchannelID;
    }

    public void setBJBannerURL(String bjbannerurl) {
        this.bjbannerurl = bjbannerurl;
    }

    public String getBJBannerURL() {
        return bjbannerurl;
    }

    public void setThumnailURL(String thumnailurl) {
        this.thumnailurl = thumnailurl;
    }

    public String getThumnailurl() {
        return thumnailurl;
    }

    public void setAfracaURL(String africaurl) {
        this.africaurl = africaurl;
    }

    public String getAfracaURL() {
        return africaurl;
    }

    public void setPartnertype(String partnertype) {
        this.partnertype = partnertype;
    }

    public String getPartnertype() {
        return partnertype;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }


    @Override
    public String toString() {
        return "[bjImage - " + thumnailurl + "] [bjname - " + bjname + "] [bjsogae - " + bjsogae + "] " +
                "[bjsubscriberCount - " + bjsubscriberCount + "] [favorite - " + favorite + "] [bjchannelID - " + bjchannelID + "] " +
                "[ bjbannerurl - " + bjbannerurl + "]" + "[ partnertype - " + partnertype + "]" + "[ type - " + type + "]";
    }
}
