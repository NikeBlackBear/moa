package com.loginsoft.moabj.model;

/**
 * Created by Administrator on 2015-02-11.
 */
public class PlayListInfo {
    private String playlistID;
    private String gettitle;
    private String defaultimage;
    private String itemcount;
    private String publishedAt;
    private String nextpagetoken;
    private String totalresult;

    public void setPlayListID(String playlistID) {
        this.playlistID = playlistID;
    }

    public String getPlaylistID() {
        return playlistID;
    }

    public void setTitle(String title) {
        gettitle = title;
    }

    public String getTitle() {
        return gettitle;
    }

    public void setDefaultImage(String defaultimage) {
        this.defaultimage = defaultimage;
    }

    public String getDefaultimage() {
        return defaultimage;
    }

    public void setItemCount(String itemcount) {
        this.itemcount = itemcount;
    }

    public String getItemCount() {
        return itemcount;
    }

    public void setPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;
    }

    public String getPublishedAt() {
        return publishedAt;
    }

    public void setNextPageToken(String token) {
        nextpagetoken = token;
    }

    public String getNextpagetoken() {
        return nextpagetoken;
    }

    public void setTotalResult(String result) {
        totalresult = result;
    }

    public String getTotalResult() {
        return totalresult;
    }
}
