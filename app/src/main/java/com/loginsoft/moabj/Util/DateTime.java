package com.loginsoft.moabj.Util;

import android.content.Context;

import com.loginsoft.moabj.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Administrator on 2015-02-02.
 */


public class DateTime {

    //현재 ( 년, 월, 일, 시, 분, 요일, am/pm )  을 받아오는 메소드
    public static String getCurrentTime(String string) {
        String returnstr = null;
        long now = System.currentTimeMillis();
        Date mDate = new Date(now);
        SimpleDateFormat year = new SimpleDateFormat("yyyy");
        SimpleDateFormat month = new SimpleDateFormat("MM");
        SimpleDateFormat date = new SimpleDateFormat("dd");
        SimpleDateFormat hour = new SimpleDateFormat("HH");
        SimpleDateFormat minute = new SimpleDateFormat("mm");
        Calendar cal = Calendar.getInstance();
        cal.setTime(mDate);
        if (string.equals("year")) {
            returnstr = year.format(mDate);
        }
        if (string.equals("month")) {
            returnstr = month.format(mDate);
        }
        if (string.equals("date")) {
            returnstr = date.format(mDate);
        }
        if (string.equals("hour")) {
            returnstr = hour.format(mDate);
        }
        if (string.equals("hour12")) {
            int i;
            if (12 < Integer.parseInt(hour.format(mDate))) {
                i = (Integer.parseInt(hour.format(mDate)) - 12);
            } else {
                i = Integer.parseInt(hour.format(mDate));
            }
            returnstr = String.valueOf(i);
        }
        if (string.equals("minute")) {
            returnstr = minute.format(mDate);
        }
        if (string.equals("day")) {
            returnstr = dayconverter(cal.get(cal.DAY_OF_WEEK));
        }
        if (string.equals("ampm")) {
            if (cal.get(cal.AM_PM) == cal.AM) {
                returnstr = "AM";
            } else {
                returnstr = "PM";
            }
        }
        return returnstr;
    }

    public static String dayconverter(int day) {
        String conversionDay = null;
        switch (day) {
            case 1:
                conversionDay = "일";
                break;

            case 2:
                conversionDay = "월";
                break;

            case 3:
                conversionDay = "화";
                break;

            case 4:
                conversionDay = "수";
                break;

            case 5:
                conversionDay = "목";
                break;

            case 6:
                conversionDay = "금";
                break;

            case 7:
                conversionDay = "토";
        }
        return conversionDay;
    }

    //현재날짜를 기준으로 업로드 날짜와 몇일 차이가 나는지 비교하여 해당 텍스트를 리턴 하는 메소드
    //현재날짜와 업로드 날짜가 같으면 "오늘" 을 리턴
    //현재날짜와 업로드 날짜의 차가 1이면 "어제" 를 리턴
    //나머지 경우에는 "x일 전" 리턴
    public static String getUploadDateCompareCurrentDate(Context context, String targetDate) {
        try {
            String result = "";
            String[] splitTargetDate = targetDate.split("-");

            Calendar calendar = Calendar.getInstance(Locale.KOREA);
            int currentYear = calendar.get(Calendar.YEAR);
            int currentDayOfYear = calendar.get(Calendar.DAY_OF_YEAR);

            calendar.set(Integer.parseInt(splitTargetDate[0]), Integer.parseInt(splitTargetDate[1]) - 1, Integer.parseInt(splitTargetDate[2]));
            int targetYear = calendar.get(Calendar.YEAR);
            int targetDayOfYear = calendar.get(Calendar.DAY_OF_YEAR);

            if (targetYear == currentYear) {
                if ((currentDayOfYear - targetDayOfYear) == 0) {
                    result = context.getString(R.string.str_today);
                } else if ((currentDayOfYear - targetDayOfYear) == 1) {
                    result = context.getString(R.string.str_yesterday);
                } else {
                    result = context.getString(R.string.str_from_day, (currentDayOfYear - targetDayOfYear));
                }
            } else {
                int compareDay = currentDayOfYear + (365 - targetDayOfYear);
                for (int i = 1; i < currentYear - targetYear; i++) {
                    compareDay += 365;
                }
                result = context.getString(R.string.str_from_day, compareDay);
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return "?";
        }
    }
}
