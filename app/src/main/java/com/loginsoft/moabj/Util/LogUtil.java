package com.loginsoft.moabj.Util;


/**
 * Created by Administrator on 2015-02-02.
 */
public class LogUtil {
    private final static String TAG = "MoaBJ";
    private final static boolean DEBUG = false;

    public static void LOGV(String msg) {
        if (DEBUG) {
            android.util.Log.v(TAG, msg);
        }
    }

    public static void LOGE(String msg) {
        if (DEBUG) {
            android.util.Log.e(TAG, msg);
        }
    }

    public static void LOGD(String msg) {
        if (DEBUG) {
            android.util.Log.d(TAG, msg);
        }
    }
}
