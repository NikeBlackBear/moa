package com.loginsoft.moabj.Util;

import android.content.Context;
import android.content.SharedPreferences;

import com.loginsoft.moabj.common.CommonValues;

/**
 * Created by Administrator on 2015-03-04.
 */
public class MoaBJPreferenceManager implements CommonValues {

    // 값 저장하기
    public static void saveAccountName(Context context, String accountName) {
        SharedPreferences accountNamePreference = context.getSharedPreferences(PREF_ACCOUNT_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = accountNamePreference.edit();
        editor.putString(ACCOUNT_NAME, accountName);
        editor.commit();
    }

    // 로그인 값 불러오기
    public static String getAccountName(Context context) {
        SharedPreferences accountNamePreference = context.getSharedPreferences(PREF_ACCOUNT_NAME, Context.MODE_PRIVATE);
        return accountNamePreference.getString(ACCOUNT_NAME, SIGN_OUT_STATE);
    }

    // 로그인 값 불러오기
    public static String getLoginState(Context context) {
        SharedPreferences loginPreference = context.getSharedPreferences(PREF_LOGIN, Context.MODE_PRIVATE);
        return loginPreference.getString(LOGIN_KEY, SIGN_OUT);
    }

    // 로그인 값 저장하기
    public static void saveLoginState(Context context, String loginState) {
        SharedPreferences loginPreference = context.getSharedPreferences(PREF_LOGIN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = loginPreference.edit();
        editor.putString(LOGIN_KEY, loginState);
        editor.commit();
    }

    // 값 불러오기
    public static String getAlarmState(Context context) {
        SharedPreferences alarmPreference = context.getSharedPreferences(PREF_ALRAM, Context.MODE_PRIVATE);
        return alarmPreference.getString(ALARM_STATE, ALARM_SWITCH_ON);
    }

    // 값 저장하기
    public static void saveAlarmState(Context context, String alarmState) {
        SharedPreferences alarmPreference = context.getSharedPreferences(PREF_ALRAM, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = alarmPreference.edit();
        editor.putString(ALARM_STATE, alarmState);
        editor.commit();
    }
}
