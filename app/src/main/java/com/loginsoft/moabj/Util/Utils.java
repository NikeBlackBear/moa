package com.loginsoft.moabj.Util;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.widget.Toast;

import com.loginsoft.moabj.R;
import com.loginsoft.moabj.http.ServerProxy;

/**
 * Created by Administrator on 2015-02-27.
 */
public class Utils {
    public static String getVersion(Context context) {
        String version = "";
        try {
            PackageInfo i = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = i.versionName;
        } catch (PackageManager.NameNotFoundException e) {
        }
        return version;
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager con = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mobile = con.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo wifi = con.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (mobile.isConnected() || wifi.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    public static void showVideo(Context context, String videoID, String bjicon) {
        if (videoID != null && !videoID.equals("")) {
            try {
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://www.youtube.com/watch?v=" + videoID));
                context.startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(context, R.string.str_not_exist_youtube, Toast.LENGTH_SHORT).show();
            }
            ServerProxy.getInstance(context).requestViewCountUp(videoID, bjicon);
        } else {
            LogUtil.LOGD("YoutubeConnector :: showVideo() :: invalid videoID");
        }
    }
}
