package com.loginsoft.moabj.Util;

import android.content.Context;
import android.graphics.Typeface;

import com.loginsoft.moabj.common.CommonValues;

/**
 * Created by Administrator on 2015-02-10.
 */
public class FontFactory implements CommonValues {
    private static FontFactory mInstance;
    private Typeface mThinNanumGothic;
    private Typeface mNanumGothic;

    private FontFactory(Context context) {
        mThinNanumGothic = Typeface.createFromAsset(context.getAssets(), FONT_THIN_NANUM_GOTHIC);
        mNanumGothic = Typeface.createFromAsset(context.getAssets(), FONT_NANUM_GOTHIC);
    }

    public static FontFactory getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new FontFactory(context);
        }
        return mInstance;
    }

    public Typeface get365AttentionFont() {
        return mThinNanumGothic;
    }

    public Typeface getTypeface(String fontName) {
        if (fontName.equals(FONT_THIN_NANUM_GOTHIC)) {
            return mThinNanumGothic;
        } else if (fontName.equals(FONT_NANUM_GOTHIC)) {
            return mNanumGothic;
        } else {
            return mNanumGothic;
        }
    }
}
